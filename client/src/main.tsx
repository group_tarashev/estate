import ReactDOM from "react-dom/client";
import App from "./App.tsx";
import "./index.css";
import { BrowserRouter } from "react-router-dom";
import { QueryClient, QueryClientProvider } from "react-query";
import AppContextProvider from "./context/AppContext.tsx";
import { SocketContextProvider } from "./context/SocketContext.tsx";
import ConversationConvextProvider from "./context/ConversationConvext.tsx";
import { CookiesProvider } from "react-cookie";

const clientQuery = new QueryClient();
ReactDOM.createRoot(document.getElementById("root")!).render(
  <QueryClientProvider client={clientQuery}>
    <CookiesProvider>
      <AppContextProvider>
        <ConversationConvextProvider>
          <SocketContextProvider>
            <BrowserRouter>
              <App />
            </BrowserRouter>
          </SocketContextProvider>
        </ConversationConvextProvider>
      </AppContextProvider>
    </CookiesProvider>
  </QueryClientProvider>
);
