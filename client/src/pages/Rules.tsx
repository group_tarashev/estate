
const Rules = () => {
  return (
    <div className="">
      <div className="pt-24 px-10 min-h-screen flex flex-col items-center m-auto justify-center p-10 max-w-[660px] gap-10">
        <h2 className="self-center mb-12 text-2xl font-bold ">
          ОБЩИ УСЛОВИЯ за ползване на сайта noagencyestate.bg
        </h2>
        <ul className="flex-col flex gap-10 justify-center items-center ">
          <li>
            1. Настоящите общи условия уреждат отношенията между
            noagencyestate.bg и лицата- ползватели на услугите на сайта
            noagencyestate.bg
          </li>
          <li>
            2. noagencyestate.bg е сайт за малки обяви в областта на пазарите,
            свързани с недвижими имоти.
          </li>
          <li>
            3. Всяко лице, което е заредило на браузъра си noagencyestate.bgили
            е било пренасочено към него, следва да се запознае внимателно с
            настоящите Общи условия и ако лицето не желае да стане ползвател на
            услугите на noagencyestate.bg, ако не желае да предоставя данни и
            информация и да има достъп до данни и информация, следва незабавно
            да напусне сайта.
          </li>
          <h2 className="font-semibold text-2xl">ОБЯВИ</h2>
        </ul>
        <ul className="flex-col flex gap-10 justify-center items-center">
          <li>
            1. Обяви могат да публикуват както лица, които желаят да сключат
            единична сделка (наричани по-нататък "частни лица").
          </li>
          <li>
            2. Всяка обява следва да съдържа информация само за един имот.
          </li>
          <li>
            3. Всеки потребител може да публикува само една обява за един
            конкретен имот. Не се допуска публикуването на дублирани обяви.
          </li>
          <li>
            4. Задължително е правилното и точно попълване на всички полета във
            формата за записване на обява, в противен случай администраторите на
            сайта си запазват правото за редактиране, променяне на позицията и
            изтриване на обяви с грешно или неправилно попълнено съдържание.
            Може да се публикува само информация, която се отнася конкретно за
            обявения имот.
          </li>
          <li>
            5. Обявите, публикувани в нарушение на тези Общи условия, независимо
            дали са публикувани безплатно или срещу заплащане, се изтриват без
            право на компенсация.
          </li>
          <li>
            6. Сайтът noagencyestate.bg има право да забрани публикуването на
            обяви от определени телефони, IP- адреси или e-mail адреси, от които
            са извършвани системни нарушения на правилата за публикуване на
            обяви в noagencyestate.bg.
          </li>
          <li>
            7. noagencyestate.bg не носи отговорност за верността и актуалността
            на публикуваните обяви. Цялата отговорност за верността и
            актуалността на публикуваните обяви и реклама се носи от лицето,
            което ги публикува. С факта на публикуването публикуващият
            декларира, че в обявите или рекламата не се съдържа подвеждаща
            информация.
          </li>
          <li>
            8. Частните лица могат да публикуват обяви в noagencyestate.bg само
            след регистрация с имейл и парола или използвайки свой регистриран
            профил . Частните лица имат право да публикуват БЕЗПЛАТНО една обява
            в сайта noagencyestate.bg(безплатна обява). noagencyestate.bg може
            да налага ограничения относно безплатните обяви. Тези ограничения
            могат да са свързани с: отложено публикуване на обявата или част от
            нея, срок на присъствие в сайта, други.
          </li>
          <li>
            9. Срещу допълнително заплащане обявите на частните лица могат да
            получат статут КОМБИНИРАНА (КОМБИНИРАНИ-обяви) или ТОП (ТОП-оферти).
            По смисъла на раздел IV, т.2.1. КОМБИНИРАНИТЕ обяви са ограничени от
            срок за присъствие в сайта и НЕ СЕ ползват с преференциално
            позициониране спрямо останалите обяви. ТОП-офертите са ограничени от
            срок за присъствие в сайта и се ползват с определени преференции при
            визуализиране, спрямо останалите обяви в noagencyestate.bg.
          </li>
          <li>
            10. Профили без активни обяви за последната 1 година се изтриват
            автоматично.
          </li>
          <li>
            11. Права за собственост върху профила не могат да се предоставят
            или прехвърлят без изричното съгласие на титуляра-собственик на
            профила.
          </li>
          <li>
            12. Право на безплатна или комбинирана обява в noagencyestate.bg,
            имат само потребители рекламиращи собствен имот, който не е придобит
            с инвестиционна цел.
          </li>
          <li>
            13. Всяка обява може да бъде коригирана или изтрита. Корекцията на
            обявата не променя нейния статут. Статутът се променя само с
            корекция на самия статут. Корекцията на информация в обява или
            нейното изтриване се извършва незабавно (on-line) и не се заплаща.
          </li>
          <li>
            14. Срещу допълнително заплащане и по ред определен в сайта
            noagencyestate.bg всяка обява може да получи статут ТОП-оферта.
            ТОП-офертите (без тези от категория Бизнес имот) се публикуват в
            поне още един сайт за малки обяви на Резон. ТОП офертите ползват
            определени преференции при визуализиране, спрямо останалите обяви в
            noagencyestate.bg.
          </li>
          <li>
            15. Всяка публикувана обява може да бъде докладвана за нередност от
            регистриран ползвател на услугите на noagencyestate.bg. След
            проверка, noagencyestate.bg има право да включи докладваната обява в
            списък на обявите, които по избор на търсещия се изключват от
            резултатите на търсенето.
          </li>
          <li>
            16. При изпращане на имейли чрез noagencyestate.bg, съдържанието не
            трябва да съдържа обидни и/или дискриминационни квалификации.
            Подобно поведение не се толерира от noagencyestate.bg. Профилите на
            потребителите пренебрегващи това условие, може да бъдат блокирани,
            без право на повторно активиране.
          </li>
          <li>
            17. Активирането на безплатна обява е възможно не по-рано от 30 дни
            от момента, в който потребителя е имал активна безплатна обява.
          </li>
        </ul>
        <h2 className="text-2xl font-semibold">ПЛАЩАНИЯ</h2>
        <ul className="flex-col flex gap-10 justify-center items-center">
          <li>
            1. Плащанията за услуги, предлагани чрез сайта noagencyestate.bg от
            юридически лица се извършват в брой или по банков път.
          </li>
          <li>
            2. Плащанията от физически лица могат да се извършват чрез EasyPay,
            ePay.bg, с банков превод, на банкомат, PayPal.com или чрез мобилните
            оператори на територията на Р България посредством технологията SMS
            (Short Message Services).
          </li>
          <li>
            3. Цените на услугите се обявяват от сайта. noagencyestate.bg не е
            мобилен оператор или оператор на плащания чрез технологията SMS
            (SMS-плащания) и не носи отговорност за недоставени или неправилно
            доставени SMS-съобщения.
          </li>
          <li>
            4. Неусвоените суми за платени услуги, предоставяни от
            noagencyestate.bg, не се възстановяват.
          </li>
          <li>
            5. Във връзка с предоставяните услуги на сайта noagencyestate.bg, не
            приема плащания в брой.
          </li>
          <li>
            6. При промяна в цените за публикуване и промотиране на обяви,
            noagencyestate.bg не дължи компенсации и не възстановява вече
            платени суми.
          </li>
        </ul>
        <h2 className="text-2xl font-semibold">ОБЩИ РАЗПОРЕДБИ</h2>
        <ul className="flex-col flex gap-10 justify-center items-center">
          <li>
            1. Настоящите Общи условия могат да бъдат променяни по всяко време
            от noagencyestate.bg с оглед повишаване качеството на предоставяните
            услуги като и с въвеждането на нови такива. Промените могат да
            произтичат и от изменения в българското законодателство.
          </li>
          <li>
            2. noagencyestate.bg си запазва правото да променя Общите условия по
            всяко време и по своя преценка. Промените се поместват на
            официалната страница на сайта и влизат в сила от деня на
            поместването им там. Ползвателите на услугите на сайта са длъжни да
            проверяват актуализациите на Общите условия и да съобразяват
            поведението си с тях.
          </li>
          <li>
            3. За всички неуредени в настоящите Общи условия въпроси се прилагат
            разпоредбите на действащото в Р. България законодателство.
          </li>
          <li>
            4. Всички възникнали спорове в отношенията между ползвателите на
            услугите на сайта и noagencyestate.bg се разрешават по взаимно
            съгласие, а ако такова не се постигне- пред компетентните съдилища в
            гр. София, съобразно приложимото в Р.България законодателство.
          </li>
        </ul>
        <h2 className="text-2xl font-semibold">"Бисквитки" – Cookies</h2>
        <ul className="flex-col flex gap-10 justify-center items-center">
          <li>
            1. "Бисквитките" (cookies) са малки файлове, които се съхраняват
            временно на вашия твърд (hard) диск и позволяват на сайта ни да
            разпознае компютъра ви следващия път, когато посетите Интернет
            страницата ни.
          </li>
          <li>
            2. Google Adwords ремаркетинг реклама. Този сайт използва
            ремаркетинг рекламна услуга на Google. Това означава, че когато
            посетите сайта, Google взима информация за посещението ви и след
            това изпраща наши реклами до нашите посетители в сайтовете които
            посещавате. Доставчици от трети страни включително Google използват
            "бисквитки" за да събират информацията за вашите посещения и я
            използват за да ви показват наши реклами в рекламните си площадки.
            Посетителите могат да се откажат от използването на „бисквитки“ от
            Google и доставчици от трети страни, на страницата за отказване на
            Инициативата за мрежова реклама{" "}
            <a
              className="text-blue-500 underline"
              href="http://www.networkadvertising.org/choices/"
            >
              http://www.networkadvertising.org/choices/
            </a>{" "}
            .
          </li>
          <li>
            3. Бисквитки на трета страна (Third Party Cookies). Този сайт
            използва бисквитки за запаметяване на информация за отчитане на
            ефективността на сайта и рекламите по демографски признак,
            интереси,продължителност на сесиите и т.н. Този сайт използва Google
            Analytics, услуга за уеб анализ, предлагана от Google, Inc. Google
            Analytics използва бисквитки, както и файлове, съхранявани на
            компютъра ви, за да се направи възможен анализ на използването на
            сайта. Цялата тази информация се използва от нас само за
            статистически цели и за разпознаване на грешни линкове или програмни
            грешки. Разкриване на информация на трети лица не се извършва.
          </li>
          <li>
            4. Можете да контролирате и/или изтривате бисквитки когато пожелаете
            - за повече информация вижте AllAboutCookies.org. Можете да изтриете
            всички бисквитки, които вече са запазени на вашия компютър, а също
            така можете да настроите повечето браузъри да ги блокират. Ако
            направите това обаче, може да се наложи ръчно да настройвате някои
            параметри всеки път, когато посещавате даден сайт, а освен това е
            възможно някои услуги и функции да не работят. Разрешаването на
            бисквитките не е абсолютно необходимо, за да може уебсайтът да
            работи, но ще допринесе за по-доброто му използване.
          </li>
        </ul>
        <h2 className="text-2xl font-semibold">Защита на личните данни</h2>
        <ul className="flex-col flex gap-10 justify-center items-center">
          <li>
            1. Идентификационни данни на администратора и координати за връзка
          </li>
          <li>
            2. Цели на обработване. Целта на събирането и обработването на данни
            е предлагането на услуги и инструменти, където потребителите могат
            да участват в дейности, включително но не само да се регистрират и
            да създадат профил, да публикуват или да разглеждат списъци с онлайн
            обяви, да комуникират с останалите потребители, да си разменят
            съобщения и да изпълняват множество свързани функции на сайта
            noagencyestate.bg. Личните данни са информация, която служи за
            Вашата идентификация, т.е. име, електронна поща. noagencyestate.bg,
            под никаква форма няма да събира Ваши лични данни, освен
            необходимите за извършване на действия по уеб сайта,
            noagencyestate.bg.
          </li>
          <li>
            3. Категории лични данни които ще се обработват. Услугите, които
            предоставя сайта noagencyestate.bg предполагат предоставяне на Лични
            данни като име, адрес, телефон , e-mail. noagencyestate.bg не събира
            данни с категория специални данни по смисъла на чл. 9 и 10 от
            Регламент (ЕС) 2016/679 . С това съгласие вие се задължавате да не
            съхранявате и изпращате такива данни чрез сайта, като например -
            данни, разкриващи Вашия расов или етнически произход, политически
            възгледи, религиозни или философски убеждения, членство в синдикални
            организации, нито Ваши генетични данни, биометрични данни, данни за
            здравословното Ви състояние, за сексуалния Ви живот или сексуалната
            Ви ориентация, или данни, свързани с Ваши присъди и нарушения. -
            документи за самоличност, както и ЕГН, номер на паспорт/лична карта
            и др. подобни.
          </li>
          <li>
            4. Категории получатели на данните. Функцията на noagencyestate.bg е
            информационна платформа за малки обяви, която позволява продавачи и
            купувачи да се свързват чрез тези обяви. С публикуването на обява
            вие предоставяте възможност на други потребители на сайта да
            разглежда вашата обява, в която вие сами определяте каква информация
            да съдържа. Данните за контакт на Администраторите на агенции ще се
            ползват само от служители на noagencyestate.bg за връзка с тях.
          </li>
          <li>
            <ul className="flex-col flex gap-10 justify-center items-center">
              <li>5. Информация за правата на субекта на данни</li>
              <li>
                - noagencyestate.bg ви осигурява непрекъснат 24 часов достъп до
                вашия акаунт, освен в случаите на техническа неизправност.
              </li>
              <li>
                - Чрез достъпа до вашия акаунт , вие имате право на проверка и
                промяна на предоставената от вас информация.
              </li>
              <li>
                - Имате възможност и да се откажете от съгласието за обработване
                и да изтриете данните ви чрез закриване на акаунта ви (като
                подадете искане за закриване на акаунта ви на e-mail:
                iliyan.tarashev@gmail.com ).
              </li>
              <li>
                - Вие имате право да възразите срещу обработването на личните ви
                данни, което се основава на задача от обществен интерес,
                упражняване на официално правомощие или на легитимен интерес.
              </li>
            </ul>
          </li>
          <li>
            6. Задължително ли е предоставянето на лични данни Задължителната
            информация, за да се регистрирате е eлектронната ви поща, а при
            публикуване на обява и телефон. Предоставянето на личните ви данни в
            останалата функционална част на сайта е напълно доброволно по ваше
            желание. Физическото лице търсещо или предлагащо чрез онлайн обяви в
            noagencyestate.bg самостоятелно преценява дали и какви данни за себе
            си да предостави.
          </li>
          <li>
            7. Автоматизирано вземане на решения, включително профилиране
            "Бисквитките" (cookies) са малки файлове, които се съхраняват
            временно на вашия твърд (hard) диск и позволяват на сайта ни да
            разпознае компютъра ви следващия път, когато посетите интернет
            страницата noagencyestate.com. noagencyestate.bg използва
            "бисквитки" единствено, за да събира информация относно ползването
            на нашите сайтове, с цел по-бързото Ви иденфициране и предоставяне
            на услуги. Можем да позволим на трети страни, като доставчици на
            реклами или анализи, да събират информация, използвайки тези
            технологии, директно от нашия уебсайт. Повече информация за
            управление на бисквитките - www.allaboutcookies.org е полезен ресурс
            с много подробна информация за бисквитките и как да ги управлявате в
            случай, че желаете да изтриете или забраните получаването на
            бисквитки от noagencyestate.bg.
          </li>
          <li>
            8. Срок на съхранение noagencyestate.bg съхранява Вашите лични данни
            за срок не по-дълъг от съществуването на профила ви в уебсайта. След
            изтичането на този срок,noagencyestate.bg полага необходимите грижи
            да изтрие и унищожи всички Ваши данни, без ненужно забавяне (до 60
            дни).
          </li>
          <li>
            9. Дали данните ще се предоставят на трета държава извън ЕС
            noagencyestate.bg e сайт достъпен и от страни извън Европейският
            съюз. Това предполага, че данните, които сте предоставили в обявата
            могат да бъдат видяни от тези трети страни.
          </li>
          <li>
            <ul className="flex-col flex gap-10 justify-center items-center">
              <li>10. Сигурност от страна на noagencyestate.bg</li>
              <li>
                - Предоставените ни данни се защитават съвестно от загуба,
                унищожение, изопачаване/фалшификация, манипулация и неправомерен
                достъп или неправомерно разкриване.
              </li>
              <li>
                - noagencyestate.bg ще използва личните ви данни единствено за
                целите на техническото управление на сайта, в който сте
                регистрирани, за да ви предостави достъп до специална информация
                или за обща комуникация с вас.
              </li>
              <li>
                - noagencyestate.bg няма право да продава или търгува с Вашите
                лични данни. Служителите на noagencyestate.bg са длъжни да
                опазват поверителността на вашите данни и да спазват
                споразумение за разкриване на поверителна информация.
                noagencyestate.bg се води от приетата фирмена политика за защита
                на личните данни, която е представена в Комисията за защита на
                лични данни и се ръководи стриктно от нея при съхранение на
                личните данни, получени от регистрирани потребители в сайтовете,
                собственост на компанията.
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Rules;
