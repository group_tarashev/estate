import { useNavigate } from "react-router-dom";
import { useAppContext } from "../context/AppContext";
import { useMutation } from "react-query";
import * as apiClient from "../apiClient";
import { useEffect, useMemo, useRef, useState } from "react";
import { useForm } from "react-hook-form";
import { EstateType } from "../components/ManageForm";
import Pagination from "../components/Pagination";
export type UpdateProfile = {
  username: string;
  password: string;
  email: string;
  image: File;
  tel: string;
};

const Profile = () => {
  const { onLogin, auth } = useAppContext();
  const navigate = useNavigate();
  const fileRef = useRef<HTMLInputElement>(null);
  const [passEdit, setPassEdit] = useState(false);
  const [myEstates, setMyEstates] = useState<EstateType[]>([]);
  const [mySaveEstates, setMySaveEstates] = useState<EstateType[]>([]);
  const { register, handleSubmit, setValue , formState:{errors}} = useForm<UpdateProfile>();
  const mutatioin = useMutation(apiClient.updateProf, {
    onSuccess: (data) => {
      onLogin(data);
    },
    onError: (error) => {
      console.log(error);
    },
  });
  const mutatioinSingOut = useMutation(apiClient.signOut, {
    onSuccess: () => {
      navigate("/");
      onLogin(undefined);
    },
  });
  const mutationEstate = useMutation(apiClient.getUserEstate, {
    onSuccess: (data) => {
      if (Array.isArray(data)) {
        setMyEstates(data);
      }
      console.log(data);
    },
  });
  const saveMutate = useMutation(apiClient.getSaved, {
    onSuccess: (data) => {
      if (Array.isArray(data)) {
        setMySaveEstates(data);
      }
      // console.log(data)
    },
  });
  const removeSaveMutate = useMutation(apiClient.removeSaved)
  const deleteMutate = useMutation(apiClient.deleteEstate);
  useEffect(() => {
    mutationEstate.mutateAsync();
    saveMutate.mutateAsync();
  }, []);
  // console.log(mutationEstate.data)
  const handleSignOut = () => {
    mutatioinSingOut.mutate();
  };
  const handleDelete = (id: string) => {
    deleteMutate.mutateAsync(id)
      .then( () =>{
      mutationEstate.mutateAsync()
      .then(() =>{
        saveMutate.mutateAsync();
      })
    })
  };
  const handleRemoveSaved = (id:string) =>{
    removeSaveMutate.mutateAsync(id).then(()=>
    saveMutate.mutateAsync()
    );
  }
  const handleEdit = (id: string, item: EstateType) => {
    navigate(`/edit-estate/${id}`, { state: item });
  };
  const onSubmit = handleSubmit(async (data) => {
    const formData = new FormData();

    formData.append("image", data.image);
    formData.set("username", data.username);
    if (data.password?.length >= 6) {
      formData.set("password", data.password);
    }
    formData.set("email", data.email);
    formData.set("tel", data.tel);
    try {
      // console.log(...formData);
      await mutatioin.mutateAsync(formData);
    } catch (error) {
      console.log(error);
    }
  });
  let pageSize = 3;
  const [currentPage, setCurrentPage] = useState(1)
  const [currentPageSaved, setCurrentPageSaved] = useState(1)

  const currentEstate = useMemo(() =>{
    const firstPage = (currentPage - 1) * pageSize;
    const lastPage = firstPage + pageSize;
    return myEstates.slice(firstPage, lastPage)
  },[currentPage, myEstates])
  const currSavedEstate = useMemo(() =>{
    const firstPage = (currentPageSaved - 1) * pageSize
    const lastPage = firstPage + pageSize;
    return mySaveEstates.slice(firstPage, lastPage)
  },[currentPageSaved, mySaveEstates])
  return (
    <>
    <div className="flex items-center justify-around bg-slate-400 flex-row sm:flex-col xl:p-10 xl:justify-around xl:gap-5 sm:p-1 pt-16 xl:pt-16 md:pt-24 sm:pt-28 h-full">
      <div className="flex flex-col w-[340px] items-center gap-5 bg-slate-200 p-4 justify-center shadow-xl rounded">
        <h2 className="text-3xl mb-10">Профил</h2>
        <form className="flex flex-col gap-5 w-full" onSubmit={onSubmit}>
          <div className="flex flex-col items-center justify-center">
            <img
              src={auth?.avatar}
              alt=""
              onClick={() => {
                fileRef.current && fileRef.current.click();
              }}
              className="w-20 h-20 object-cover rounded-full "
            />
            <input
              {...register("image")}
              type="file"
              hidden
              max={1}
              accept="image/*"
              ref={fileRef}
              onChange={(e) => {
                if (e.target.files) {
                  setValue("image", e.target.files[0]);
                }
              }}
            />
          </div>
          <input
            className="border p-1 w-full"
            type="text"
            placeholder={auth?.username}
            {...register("username")}
            defaultValue={auth?.username}
          />
          <input
            className="border p-1"
            type="email"
            placeholder={auth?.email}
            {...register("email")}
            defaultValue={auth?.email}
          />
          <input
            className="border p-1"
            type="text"
            placeholder={auth?.tel}
            {...register("tel", {required: "Телефон е задължителен"})}
            defaultValue={auth?.tel}
          />
          {errors.tel && <span>{errors.tel.message}</span>}
          <input
            disabled={!passEdit}
            type="password"
            className="border p-1"
            placeholder="password"
            {...register("password")}
          />
          <label htmlFor="pass" className="flex items-center gap-2">
            <input
              type="checkbox"
              id="pass"
              className="w-5 h-5"
              onChange={(e) => setPassEdit(e.target.checked)}
            />
            <span>Промени парола</span>
          </label>
          <button
            disabled={mutatioin.isLoading}
            type="submit"
            className="w-full bg-white p-1"
          >
            {mutatioin.isLoading ? "Обновяване..." : "Обнови"}
          </button>
        </form>
        <button
          onClick={() => navigate("/add-estate")}
          className="bg-green-400 p-2 w-full"
        >
          Добави Обява
        </button>
        <button
          onClick={handleSignOut}
          className="bg-red-500 text-white  mt-10 p-1 w-full"
        >
          Излез
        </button>
      </div>
      <div className="flex flex-col gap-20 sm:w-full sm:p-1 xl:max-w-[400px] ">
        <div className="flex flex-col my-10 gap-5 flex-3 w-[500px] sm:w-full xl:w-full h-[220px]">
          <h2 className="text-xl self-center">Моите обяви</h2>
          {mutationEstate.isLoading ||
            (myEstates &&
              currentEstate.map((item: EstateType) => (
                <div
                  key={item._id}
                  className="border border-slate-700 p-1 flex w-full justify-between items-center rounded shadow-xl sm:w-full bg-slate-200"
                >
                  <div
                    className="flex items-center gap-2 cursor-pointer"
                    onClick={() =>
                      navigate(`/estate/${item._id}`, { state: item })
                    }
                  >
                    <img
                      className="w-12 h-12 object-cover"
                      src={item?.imagesUrls[0]}
                      alt=""
                    />
                    <span className="text-[16px]">
                      {item.name.substring(0, 35)}
                    </span>
                  </div>
                  <div className="flex gap-5 sm:flex-col">
                    <button
                      className="text-red-500"
                      onClick={() => handleDelete(item._id)}
                    >
                      Изтрий
                    </button>
                    <button
                      onClick={() => handleEdit(item._id, item)}
                      className="text-green-500"
                    >
                      Редактирай
                    </button>
                  </div>
                </div>
              )))}
        </div>
              <Pagination
                currentPage={currentPage}
                totalCount={myEstates.length}
                pageSize={pageSize}
                onPageChange={page => setCurrentPage(page)}
                siblingCount={1}
              />
        <div className="flex flex-col gap-5">
          <h2 className="text-xl self-center">Запазени обяви</h2>
          <div className="flex flex-col gap-4 h-[220px] my-10">
            {saveMutate.isLoading ||
              mySaveEstates && currSavedEstate?.map((item: EstateType) => (
                <div
                  key={item._id}
                  className="border border-slate-700 p-1 flex w-full justify-between items-center rounded shadow-xl bg-slate-200"
                >
                  <div
                    className="flex items-center gap-2 cursor-pointer"
                    onClick={() =>
                      navigate(`/estate/${item._id}`, { state: item })
                    }
                  >
                    <img
                      className="w-10 h-10 object-cover"
                      src={item?.imagesUrls[0]}
                      alt=""
                    />
                    <span className="text-[16px]">
                      {item.name.substring(0, 35)}
                    </span>
                  </div>
                  <button onClick={() => handleRemoveSaved(item._id)}>Премахни</button>
                </div>
              ))}
          </div>
              <Pagination
                onPageChange={(page) => setCurrentPageSaved(page)}
                pageSize={pageSize}
                siblingCount={1}
                totalCount={mySaveEstates.length}
                currentPage={currentPageSaved}
              />
        </div>
      </div>
    </div>
    </>
  );
};

export default Profile;
