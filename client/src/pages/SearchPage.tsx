import { useLocation } from "react-router-dom";
import Card from "../components/Card";
import { EstateType } from "../components/ManageForm";
import { useForm } from "react-hook-form";
import { useMutation } from "react-query";
import * as apiClient from "../apiClient";
import { useEffect, useState } from "react";
const SearchPage = () => {
  const { state } = useLocation();
  //   console.log(state);
  const [estates, setEstates] = useState<EstateType[]>(state);
  const [cities, setCities] = useState<string[]>([]);
  const [allCities, setAllCities] = useState<string[]>([]);
  const { register, handleSubmit,setValue} = useForm<EstateType>();
  const { mutateAsync } = useMutation(apiClient.getSearchOptions, {
    onSuccess: (data) => {
      // console.log(data)
      setEstates(data);
    },
  });
  const onSubmit = handleSubmit((data) => {
    mutateAsync(data);
  });
  
  useEffect(() => {
    setEstates(state);
  }, [state]);
  useEffect(() => {
    apiClient.fetcSearchData(setAllCities);
  }, []);
  const handleCities = async (e: React.ChangeEvent<HTMLInputElement>) => {
    const filt = allCities.filter(city => city.toLowerCase().startsWith(e.target.value.toLowerCase())).slice(0,10)
    setCities(filt)
    if(e.target.value == ''){
      setCities([])
    }
    // console.log(cities)
  };
  return (
    <>
    <div className="flex gap-2 p-5 sm:flex-col pt-16 sm:h-full md:h-full h-full">
      <form
        className=" flex flex-col  min-w-[280px] border p-2 sticky top-16  h-full gap-4 sm:relative sm:h-full"
        onSubmit={onSubmit}
      >
        <h2 className="self-center text-xl mb-10">Опции за търсене</h2>
        <div>
          <select id="" className="w-full p-1" {...register('type')}>
            <option value="">Тип</option>
            <option value="flat">Апартамент</option>
            <option value="house">Къща/Вила</option>
            <option value="garadge">Гараж</option>
            <option value="office">Офис</option>
            <option value="land">Земеделска земя</option>
          </select>
        </div>

        <div className="flex flex-wrap gap-5  border-b-4 sm:justify-center flex-col">
          <label htmlFor="rent" className="flex gap-2">
            <input type="checkbox" {...register("rent")} id="rent" />
            <span>Под наем</span>
          </label>
          <label htmlFor="sell" className="flex gap-2">
            <input type="checkbox" {...register("sell")} id="sell" />
            <span>Продава</span>
          </label>
          <label htmlFor="mate" className="flex gap-2">
            <input type="checkbox" {...register("mate")} id="mate" />
            <span>Съквартирант</span>
          </label>
        </div>
        <div className="flex gap-5 border-b-2 sm:justify-center flex-col">
          <label htmlFor="parking" className="flex gap-2">
            <input type="checkbox" {...register("parking")} id="parking" />
            <span>Паркинг</span>
          </label>
          <label htmlFor="offer" className="flex gap-2">
            <input type="checkbox" {...register("offer")} id="offer" />
            <span>Оферта</span>
          </label>
          <label htmlFor="furnish" className="flex gap-2">
            <input type="checkbox" {...register("furnish")} id="furnish" />
            <span>Обзаведен</span>
          </label>
        </div>
        <div className="flex flex-col gap-4">
          <select className="flex gap-2 w-full p-1" {...register("sort")}>
            <option value="">Цена</option>
            <option value="1">Възходящо</option>
            <option value="-1">Низходящо</option>
          </select>
          <label htmlFor="address" className="flex gap-2 p-1 items-center">
            <span>Град/Село</span>
            <input
              className="border p-1 w-32"
              type="text"
              {...register("address")}
              id="address"
              onChange={handleCities}
            />
          </label>
          
          {cities.length > 0 && <div className="flex flex-col gap-2 bg-slate-200 p-1">{
            cities.map((item, i) =>(
              <div onClick={() => {setValue('address', item); setCities([])}} className="cursor-pointer bg-white p-1" key={i}>{item}</div>
            ))
            }</div>}
        </div>
        <button
          type="submit"
          className="bg-green-400 text-white font-bold w-full p-2"
        >
          Търси
        </button>
      </form>
      <div className="flex-2 flex w-full flex-col sm:justify-center sm:items-center sm:w-full">
        <h2 className="self-center text-3xl mb-10 ">Резултати</h2>
        <div className="flex flex-wrap gap-5 sm:justify-center justify-center ">
          {estates &&
            estates?.map((item: EstateType) => (
              <Card estate={item} key={item._id} />
            ))}
        </div>
      </div>
    </div>
    </>

  );
};

export default SearchPage;
