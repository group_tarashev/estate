import { useLocation } from "react-router-dom";
import * as apiClient from "../apiClient";
import { useMutation } from "react-query";
import { useEffect, useState } from "react";
import { EstateType } from "../components/ManageForm";
import Card from "../components/Card";

const UserProfile = () => {
  const location = useLocation();
  const [estates, setEstates] = useState<EstateType[]>([]);
  const user = location.state.currentSender;
  const id = location.pathname.substring(6, location.pathname.length);
  console.log(location.state.currentSender);
  const { mutateAsync, isLoading } = useMutation(apiClient.getUserEstateById, {
    onSuccess: (data) => {
      console.log(data);
      setEstates(data);
    },
  });
  useEffect(() => {
    mutateAsync(id);
  }, []);
  return (
    <div className="flex flex-col p-10 gap-5 pt-24 h-full ">
      <div className="bg-slate-100 flex items-center flex-wrap rounded-lg p-5 justify-between">
        <div className="flex  items-center gap-2 ">
          <img
            src={user.avatar as string}
            className="w-10 h-10 rounded-full"
            alt={"image"}
          />
          <h1 className="font-bold">{user.username}</h1>
        </div>
        <div className="flex gap-2">
            <h1 className="font-bold">Email: </h1>
            <span>{user.email}</span>
        </div>
        <div className="flex gap-2">
            <h1 className="font-bold">Tel:</h1>
            <span>{user.tel}</span>
        </div>

      </div>
        <h1 className="self-center flex items-center gap-1"> <span>{estates.length}</span> <span>Обяви на</span> <span className="font-bold text-xl">{user.username}</span></h1>
      <div className="flex flex-wrap gap-3 p-5 justify-center bg-slate-100 rounded-lg">
        {isLoading ||
          estates.map((estate: EstateType) => (
            <Card estate={estate} key={estate._id} />
          ))}
      </div>
    </div>

  );
};

export default UserProfile;
