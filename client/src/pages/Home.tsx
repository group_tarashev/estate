import { useQuery } from "react-query";
import * as apiClient from "../apiClient";
import Card from "../components/Card";
import { EstateType } from "../components/ManageForm";
import "../style/loader.css";
import { useMemo, useState } from "react";
import Pagination from "../components/Pagination";
const Home = () => {
  const { data: estates, isLoading } = useQuery(
    "getEstates",
    apiClient.getEstates
  );
  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 9;
  const currentEstate = useMemo(() => {
    let start = (currentPage - 1) * pageSize;
    let last = start + pageSize;
    if (!isLoading) {
      return estates.slice(start, last);
    }
  }, [estates, currentPage]);
  if (isLoading) {
    return (
      <div className="flex w-full h-screen justify-center items-center">
        <span className="loading"></span>
      </div>
    );
  }
  return (
    <>

      <div className="flex gap-2 md:flex-col pt-16">
        <div className="w-[300px] h-screen p-2 md:h-full">
          {" "}
          <h2>Място за вашата реклама</h2>
        </div>
        <div className="p-10 flex mt-2 flex-wrap gap-10 bg-[#90AEAD] min-h-screen">
          {currentEstate.map((estate: EstateType) => (
            <Card key={estate._id} estate={estate} />
          ))}
        </div>
        <Pagination
          onPageChange={(page) => setCurrentPage(page)}
          pageSize={pageSize}
          siblingCount={1}
          totalCount={estates.length}
          currentPage={currentPage}
        />
      </div>
    </>
  );
};

export default Home;
