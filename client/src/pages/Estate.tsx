import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";
import { FiArrowLeft, FiArrowRight, FiNavigation } from "react-icons/fi";
import { FaBath, FaBed, FaChair, FaParking } from "react-icons/fa";
import { EstateType } from "../components/ManageForm";
import Modal from "../components/Modal";
import Images from "../components/Images";
import { useMutation } from "react-query";
import * as apiClient from "../apiClient";
import { useCookies } from "react-cookie";
const Estate = () => {
  const estate: EstateType = useLocation().state;
  const [imgIndex, setImgIndex] = useState(0);
  const [openImg, setOpenImg] = useState(false);
  const [saved, setSaved] = useState(false);
  const [cookie] = useCookies(["_au_tkn"]);
  const mutationSaved = useMutation(apiClient.getSaved, {
    onSuccess: (data) => {
      if (data) {
        if (cookie._au_tkn !== undefined) {
          const found = data.some((item: EstateType) => item._id == estate._id);
          setSaved(found);
        }
      }
    },
  });
  const mutation = useMutation(apiClient.safeEstate, {
    onSuccess: () => {
      if (cookie._au_tkn !== undefined) {
        mutationSaved.mutateAsync();
      }
    },
  });
  useEffect(() => {
    if (cookie._au_tkn !== undefined) {
      mutationSaved.mutateAsync();
    }
  }, []);
  const handleImageInc = () => {
    setImgIndex((prev) => (prev > estate.imagesUrls.length - 2 ? 0 : prev + 1));
  };
  const handleImageDcs = () => {
    setImgIndex((prev) =>
      prev <= 0 ? estate.imagesUrls.length - 1 : prev - 1
    );
  };
  const onCloseImg = () => {
    setOpenImg(false);
  };
  const handleEstate = (id: string) => {
    mutation.mutateAsync(id);
  };

  return (
    <>
      <div className="flex flex-col w-full justify-center items-center px-10 gap-10 sm:px-1 sm:mb-10 pt-16 h-screen ">
        <div className="flex relative ">
          <button className="absolute left-4 top-1/2" onClick={handleImageDcs}>
            <FiArrowLeft
              size={36}
              color="white"
              className="rounded-full bg-slate-800 p-1"
            />
          </button>
          {estate.imagesUrls.map((image, i) => (
            <img
              className={
                imgIndex == i
                  ? "h-[340px] w-screen object-cover  cursor-pointer"
                  : "hidden"
              }
              src={image.toString()}
              key={i}
              alt=""
              onClick={() => setOpenImg(true)}
            />
          ))}
          <button className="absolute right-4 top-1/2" onClick={handleImageInc}>
            <FiArrowRight
              size={36}
              color="white"
              className="rounded-full bg-slate-800 p-1"
            />
          </button>
        </div>
        <div className="flex md:flex-col">
          <div className="w-[360px] border-2 p-2">
            <h1>Място за вашата реклама</h1>
          </div>
          <div className=" m-auto w-full px-24 sm:px-1 xl:px-2 flex flex-col gap-2">
            <h2 className="text-3xl capitalize">
              {estate.name} - {Number(estate.price).toFixed(2)} лв{" "}
              {estate.sell ? "" : "/м"}
            </h2>
            <span className="flex items-center gap-3 capitalize">
              <FiNavigation /> {estate.address}
            </span>
            <div className="flex flex-wrap gap-6">
              {estate.rent && (
                <span className="py-1 px-4 bg-green-400 text-white rounded-lg">
                  Под наем
                </span>
              )}
              {estate.sell && (
                <span className="py-1 px-4 bg-green-400 text-white rounded-lg">
                  Продавам
                </span>
              )}
              {estate.mate && (
                <span className="py-1 px-4 bg-green-400 text-white rounded-lg">
                  Съквартирант
                </span>
              )}
            </div>
            <div>
              <p>{estate.description}</p>
            </div>
            <div className="flex items-center flex-wrap gap-6">
              {estate.beds && (
                <span className="flex items-center gap-1">
                  <FaBed /> {estate.beds} Легла
                </span>
              )}
              {estate.baths && (
                <span className="flex items-center gap-1">
                  <FaBath /> {estate.baths} Бани
                </span>
              )}
              {estate.parking && (
                <span className="flex items-center gap-1">
                  <FaParking /> {estate.parking && "Паркинг"}{" "}
                </span>
              )}
              {estate.furnished && (
                <span className="flex items-center gap-1">
                  <FaChair /> Обзаведен
                </span>
              )}
            </div>
            <div className="flex  self-end gap-3 items-center">
              <div className="flex items-center gap-2">
                <span>Контакт</span>
                <button className=" flex  items-center font-bold border-2 text-black p-2">
                  {estate.userTel}
                </button>
              </div>
              {cookie._au_tkn && (
                <div>
                  {saved ? (
                    <span className="bg-green-400 p-2 text-white">
                      Запазено
                    </span>
                  ) : (
                    <button
                      onClick={() => handleEstate(estate._id)}
                      className="bg-green-400 p-2"
                    >
                      Запази обявата
                    </button>
                  )}
                </div>
              )}
            </div>
          </div>
        </div>

        <Modal open={openImg} close={onCloseImg}>
          <Images images={estate.imagesUrls} />
        </Modal>
      </div>
    </>
  );
};

export default Estate;
