import { useLocation, } from "react-router-dom"
import ManageForm from "../components/ManageForm"
import { useMutation } from "react-query";
import * as apiClient from '../apiClient'
const EditEstate = () => {
    const estate = useLocation().state
    const mutation = useMutation(apiClient.updateEstateById)
    const onSave = async (data: FormData) =>{
        mutation.mutateAsync(data)
    }
  return (
    <div className="flex flex-col p-10">
        <h2 className="self-center text-3xl mb-10">Промени обява</h2>
        <ManageForm estate= {estate} onSave={onSave} isLoading={mutation.isLoading}/>
    </div>
  )
}

export default EditEstate