import { useMutation } from 'react-query';
import ManageForm from '../components/ManageForm'
import * as apiClient from "../apiClient";
import { useNavigate } from 'react-router-dom';

const AddEstate = () => {
  const navigate = useNavigate();
  const { mutateAsync, isLoading } = useMutation(apiClient.addEstate, {onSuccess: ()=>{
    navigate('/profile')
  }});

  const onSave = async (data:FormData) =>{
    mutateAsync(data)
  }
  return (
    <div className='flex flex-col items-center p-10 pt-24 h-full '>
        <h2 className='text-3xl font-bold mb-10'>Добави обява</h2>
        <ManageForm isLoading={isLoading} onSave={onSave}/>
    </div>
  )
}

export default AddEstate