import { useForm } from "react-hook-form";
import { useMutation } from "react-query";
import * as apiClient from '../apiClient'
import { useNavigate } from "react-router-dom";
import { useState } from "react";
export type RegisterForm = {
  username: string;
  password: string;
  confirmPassword: string;
  email: string;
  tel: string,
};
const Signup = () => {
  const {
    register,
    watch,
    handleSubmit,
    formState: { errors },
  } = useForm<RegisterForm>();
  const [message, setMessage] = useState<string| undefined>(undefined)
  const navigate = useNavigate();
  const mutation = useMutation("registration", apiClient.registration, {
    onSuccess:() =>{
      navigate('/sign-in');
    },
    onError:() =>{
      setMessage('User already exist')
    }
    
  } )
  const onSubmit = handleSubmit((data) =>{
    mutation.mutate(data)
  })
  return (
    <div className="flex flex-col items-center  bg-[#244855] p-10 h-screen pt-24 ">
      <h1 className="mb-10 text-3xl text-[#FBE9D0]">Регистрация</h1>
      <form action="" className="flex flex-col  gap-4 bg-[#90AEAD] p-5 rounded-lg" onSubmit={onSubmit}>
        <label htmlFor="" className="flex gap-2 items-center">
          <span className="text-sm w-24">Потр. Име</span>
          <input
            type="text"
            {...register("username", {
              required: "This field is required",
              minLength: {
                value: 6,
                message: "At least 6 chars",
              },
            
            })}
            className="border rounded p-1"
          />
          {errors.username && (<span className="text-red-500 text-sm">{errors.username.message}</span>)}
        </label>
        <label htmlFor="" className="flex gap-2 items-center">
          <span className="text-sm w-24">Email</span>
          <input
            type="email"
            {...register("email", {
              required: "This field is required",
            })}
            className="border rounded p-1"
          />
          {errors.email && (<span className="text-red-500 text-sm">{errors.email.message}</span>)}
        </label>
        <label htmlFor="" className="flex gap-2 items-center">
          <span className="text-sm w-24">Телефон</span>
          <input
            type="text"
            {...register("tel", {
              required: "This field is required",
            })}
            className="border rounded p-1"
          />
          {errors.tel && (<span className="text-red-500 text-sm">{errors.tel.message}</span>)}
        </label>
        <label htmlFor="" className="flex gap-2 items-center">
          <span className="text-sm w-24">Парола</span>
          <input
            type="password"
            {...register("password", {required:"This filed is required",
            minLength:{
                value: 6,
                message:"Най-малко 6 символа"
              }
            })}
            className="border rounded p-1"
          />
          {errors.password&& (<span className="text-red-500 text-sm">{errors.password.message}</span>)}
        </label>
        <label htmlFor="" className="flex gap-2 items-center">
          <span className="text-sm w-24">Повт. парола</span>
          <input
            type="password"
            {...register("confirmPassword", {
            minLength:{
                value: 6,
                message:"At least 6 chars required"
              },validate:(val) =>{
                if(watch('password') !== val){
                  return "Паролата не съвпада"
                }
              }
            })}
            className="border rounded p-1"
          />
          {errors.confirmPassword&& (<span className="text-red-500 text-sm">{errors.confirmPassword.message}</span>)}
        </label>
        <button type="submit" className="bg-[#FBE9D0] text-[#244855] rounded-lg font-bold p-2">Регистрация</button>
        {message && (<span className="text-red-500">{message}</span>)}
      </form>
      <div className="flex justify-around rounded-lg gap-10 mt-5 items-center bg-[#90AEAD] w-[340px] p-2">
        <p className="text-lg text-[#FBE9D0]">Имаш акаунт </p>
        <button onClick={() => navigate('/sign-in')} className="bg-[#E64833] rounded-lg text-lg text-white p-2">Впиши се</button>
      </div>
    </div>

  );
};

export default Signup;
