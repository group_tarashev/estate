import { useState } from 'react'
import { useForm } from 'react-hook-form'
import * as apiClient from '../apiClient'
import { useMutation} from 'react-query'
import { useNavigate } from 'react-router-dom'
import { useAppContext } from '../context/AppContext'
import OAuth from '../components/OAuth'
export type LoginForm = {
  email: string,
  password: string
} 
const Signin = () => {
  const {register, handleSubmit, formState:{errors}} = useForm<LoginForm>();
  const navigate = useNavigate();
  const [message, setMessage] = useState<string | undefined>(undefined)
  const {onLogin} = useAppContext();
  const mutation = useMutation(apiClient.login, {
     onSuccess: (data) =>{
       onLogin(data)
      navigate('/')
    },
    onError: (err : any) =>{
      setMessage(err.message)
      console.log(err)
    }
  })
  const onSubmit = handleSubmit((data) =>{
    mutation.mutateAsync(data)
  })
  return (
    <div className='flex flex-col items-center pt-24 bg-[#244855] p-10 h-screen'>
      <h2 className='text-3xl mb-20 text-[#FBE9D0]'>Вписване</h2>
      <form action="" className='flex flex-col gap-3 bg-[#90AEAD] shadow-md p-4 rounded-lg' onSubmit={onSubmit}>
        <label htmlFor="" className='flex gap-2 items-center'>
          <span className='w-24'>Email</span>
          <input type="text" className='border rounded ' {...register("email", {required:'Required'})}/>
        </label>
          {errors.email && (<span className='text-red-500 text-sm'>{errors.email.message}</span>)}
        <label htmlFor="" className='flex gap-2 items-center'>
          <span className='w-24'>Парола</span>
          <input type="password" className='border rounded ' {...register("password", {minLength:{
            value: 6,
            message:"Min length 6 chars"
          }})}
          />

        </label>
          {errors.password && (<span className='text-red-500 text-sm'>{errors.password.message}</span>)}
        <button type='submit' className='bg-[#FBE9D0] text-[#244855] font-bold p-2'>Влез</button>
        {message && (<span className='text-red-500'>{message}</span>)}
      </form>
      <div className='flex gap-10 mt-5'>
      <button className='bg-[#FBE9D0] text-[#244855] p-2 font-bold rounded-lg' onClick={() => navigate('/sign-up')}>Регистрирай се</button>
        <OAuth/>
      </div>
    </div>

  )
}

export default Signin