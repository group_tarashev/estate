import SideBar from '../components/messages/SideBar'
import MessageContainer from '../components/messages/MessageContainer'

const Messages = () => {
  // const [open, setOpen] = useState(false);
  return (
    <div className='flex justify-start md:h-full h-screen  md:flex-col w-full  pt-16 relative  p-2 gap-2'>
        {/* <button className={open ?"hidden":"absolute left-4 top-18"} onClick={()=> setOpen(true)}><FiMenu size={32}/></button> */}
        <SideBar />
        <div >
          <MessageContainer/>
        </div>
    </div>
  )
}

export default Messages