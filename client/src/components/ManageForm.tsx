import { useForm } from "react-hook-form";
import  { useEffect } from "react";
export type EstateType = {
  _id: string;
  name: string;
  description: string;
  address: string;
  type: string;
  etaj: number;
  sell: boolean;
  kv: number;
  furnish: boolean;
  water: boolean;
  power: boolean;
  rent: boolean;
  mate: boolean;
  parking: boolean;
  furnished: boolean;
  offer: boolean;
  beds: number;
  baths: number;
  price: string;
  imagesFiles: File[];
  imagesUrls: string[];
  sort: number;
  userTel: string;
};
type Props = {
  estate?: EstateType;
  onSave: (data: FormData) => void;
  isLoading: boolean;
};
const ManageForm = ({ estate, onSave, isLoading }: Props) => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<EstateType>();
  // console.log(estate)
  useEffect(() => {
    reset(estate);
  }, [estate, reset]);
  const onSubmit = handleSubmit((data) => {
    const formData = new FormData();
    if (estate) {
      formData.append("_id", estate._id);
    }
    formData.append("name", data.name.toLowerCase());
    formData.append("description", data.description);
    formData.append("type", data.type);
    formData.append("address", data.address.toLowerCase());
    formData.append("sell", data.sell.toString());
    formData.append("etaj", data.etaj.toString());
    formData.append("rent", data.rent.toString());
    formData.append("mate", data.mate.toString());
    formData.append("parking", data.parking.toString());
    formData.append("furnished", data.furnished.toString());
    formData.append("offer", data.offer.toString());
    formData.append("beds", data.beds.toString());
    formData.append("baths", data.baths.toString());
    formData.append("price", data.price);
    if (data.imagesFiles) {
      Array.from(data.imagesFiles)?.map((image) => {
        formData.append(`imagesFiles`, image);
      });
      // console.log(data.imagesFiles)
    }
    if (data.imagesUrls) {
      data.imagesUrls.map((image) => {
        formData.append("imagesUrls", image);
      });
    }
    onSave(formData);
  });
  // const existImages = watch("imagesUrls");
  // const handleDeleteImg = (e:React.MouseEvent<HTMLButtonElement>, imgUrl:string) =>{
  //   e.preventDefault();
  //   setValue("imagesUrls",
  //      existImages.filter((img) => img !== imgUrl)
  //   )
  // }
  return (
    <form
      className="w-full flex p-10 gap-5 bg-slate-200 md:flex-col"
      onSubmit={onSubmit}
    >
      <div className="flex flex-col flex-1 p-4 w-full gap-4">
        <div>
          <select
            id=""
            className="w-full p-1 bg-white"
            {...register("type", { required: "Изберете тип" })}
          >
            <option value="Type...">Тип</option>
            <option value="flat">Апартамент</option>
            <option value="house">Къща/Вила</option>
            <option value="garage">Гараж</option>
            <option value="office">Офис</option>
            <option value="land">Земеделска земя</option>
          </select>
        </div>
        <input
          type="text"
          placeholder="Име на имота"
          className="p-2 border"
          {...register("name", { required: "Името е задължително" })}
        />
        {errors.name && (
          <span className="text-red-500 text-sm">{errors.name.message}</span>
        )}
        <textarea
          className="border p-2"
          placeholder="Описание"
          {...register("description", {
            required: "Описанието е задължително",
          })}
        />
        {errors.description && (
          <span className="text-red-500 text-sm">
            {errors.description.message}
          </span>
        )}
        <input
          className="border p-2"
          type="text"
          placeholder="Адрес"
          {...register("address", { required: "Адресът е задължителен" })}
        />
        {errors.address && (
          <span className="text-red-500 text-sm">{errors.address.message}</span>
        )}
        <div className="flex flex-col flex-wrap gap-4">
          <div className="flex gap-5 flex-wrap md:flex-col">
            <label htmlFor="sell" className="flex items-center gap-1">
              <input
                type="checkbox"
                className="w-5 h-5"
                {...register("sell")}
              />
              <span>Продажба</span>
            </label>
            <label htmlFor="rent" className="flex items-center gap-1">
              <input
                type="checkbox"
                className="w-5 h-5"
                {...register("rent")}
              />
              <span>Под наем</span>
            </label>
            <label htmlFor="mate" className="flex items-center gap-1">
              <input
                type="checkbox"
                className="w-5 h-5"
                {...register("mate")}
              />
              <span>Съквартирант</span>
            </label>
          </div>
          <div className="flex gap-5 md:flex-col">
            <label htmlFor="parking" className="flex items-center gap-1">
              <input
                type="checkbox"
                className="w-5 h-5"
                {...register("parking")}
              />
              <span>Паркинг</span>
            </label>
            <label htmlFor="furnished" className="flex items-center gap-1">
              <input
                type="checkbox"
                className="w-5 h-5"
                {...register("furnished")}
              />
              <span>Обзаведен</span>
            </label>
            <label htmlFor="offer" className="flex items-center gap-1">
              <input
                type="checkbox"
                className="w-5 h-5"
                {...register("offer")}
              />
              <span>Оферта</span>
            </label>
            <label htmlFor="water" className="flex items-center gap-1">
              <input
                type="checkbox"
                className="w-5 h-5"
                {...register("water")}
              />
              <span>Вода</span>
            </label>
            <label htmlFor="power" className="flex items-center gap-1">
              <input
                type="checkbox"
                className="w-5 h-5"
                {...register("power")}
              />
              <span>Ток</span>
            </label>
          </div>
        </div>
        <div className="flex flex-wrap gap-4">
          <label htmlFor="beds" className="flex items-center gap-2">
            <input
              type="number"
              className="w-16 h-10 [appearance:textfield] [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:appearance-none"
              {...register("beds")}
            />
            <span>Легла</span>
            {errors.beds && (
              <span className="text-red-500 text-sm">
                {errors.beds.message}
              </span>
            )}
          </label>
          <label htmlFor="baths" className="flex items-center gap-2">
            <input
              type="number"
              className="w-16 h-10 [appearance:textfield] [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:appearance-none"
              {...register("baths", {
                minLength: { message: "Minimum 1 bath", value: 1 },
              })}
              min={1}
            />
            <span>Бани</span>
          </label>
          {errors.baths && (
            <span className="text-red-500 text-sm">{errors.baths.message}</span>
          )}
          <label htmlFor="kv" className="flex items-center gap-2">
            <input
              type="number"
              className="w-16 h-10 [appearance:textfield] [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:appearance-none"
              {...register("kv")}
            />
            <span>Квадратура</span>
          </label>
          <label htmlFor="etaj" className="flex items-center gap-2">
            <input
              type="number"
              className="w-16 h-10 [appearance:textfield] [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:appearance-none"
              {...register("etaj")}
            />
            <span>Етаж</span>
          </label>
          <label htmlFor="price" className="flex items-center gap-2 ">
            <input
              type="text"
              className="w-full p-2"
              {...register("price", { required: "Цената е задължителна" })}
            />
            <span className="w-full">Цена</span>
          </label>
          {errors.price && (
            <span className="text-red-500 text-sm">{errors.price.message}</span>
          )}
        </div>
      </div>
      <div className="flex flex-col flex-1  p-2 gap-5">
        <label htmlFor="images">Максимум 6 снимки</label>
        <input
          type="file"
          min={1}
          max={6}
          multiple
          className="border p-1"
          {...register("imagesFiles", {
            validate: (val) => {
              if (val.length < 1) {
                return "Задължително е да има снимка ";
              } else if (val.length > 6)
                return "Снимките трябва да са по-малко от 6";
            },
          })}
        />

        {errors.imagesFiles && (
          <span className="flex text-red-500">
            {errors.imagesFiles.message}
          </span>
        )}
        {isLoading ? (
          <span className="bg-slate-400 p-3 text-white font-bold self-center ">
            Зареждане
          </span>
        ) : (
          <button
            type="submit"
            className="bg-slate-400 p-3 text-white font-bold"
          >
            {estate ? "Промени обява" : "Създай Обява"}
          </button>
        )}
      </div>
    </form>
  );
};

export default ManageForm;
