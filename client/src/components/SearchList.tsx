type Props = {
    cities: string[],
    setCities : (item:string[]) => void
    setValue : (address: string, item:string) => void
}
const SearchList = ({cities, setCities, setValue} :Props) => {
  return (
    <div>
        {cities.length > 0 && <div className="flex flex-col gap-2 bg-slate-200 p-1">{
            cities.map((item, i) =>(
              <div onClick={() => {setValue('address', item); setCities([])}} className="cursor-pointer bg-white p-1" key={i}>{item}</div>
            ))
            }</div>}
    </div>
  )
}

export default SearchList