import { GoogleAuthProvider, getAuth, signInWithPopup } from "firebase/auth";
import { app } from "../firebase";
import { useMutation } from "react-query";
import * as apiClient from '../apiClient'
import { useAppContext } from "../context/AppContext";
import { useNavigate } from "react-router-dom";
export type GoogleType = {
    username: string | null,
    email: string | null,
    avatar: string | null,
    
} 
const OAuth = () => {
    const {onLogin} = useAppContext();
    const navigate = useNavigate();
    const mutation = useMutation(apiClient.signinGoogle, {onSuccess: async (data) =>{
        onLogin(data)
        navigate('/');
    }})
    const handleGoogleLogin = async () =>{
        try {
            const provider = new GoogleAuthProvider();
            const auth = getAuth(app);
            const result = await signInWithPopup(auth,provider);
            // console.log(result.user)
            mutation.mutate({
                username: result.user.displayName,
                email: result.user.email,
                avatar: result.user.photoURL
            })
        } catch (error) {
            console.log("Could not sign in with google")
        }   
    }
    if(mutation.isLoading){
        return (
            <h2 className="text-green-500 text-2xl">Loading ...</h2>
        )
    }
  return (
    <button onClick={handleGoogleLogin} className='bg-[#E64833] p-2  text-white rounded-lg'>Sign up with Google</button>
  )
}

export default OAuth