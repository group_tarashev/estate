import usePagination from '../hooks/usePagination'
import { IoIosArrowBack,IoIosArrowForward } from "react-icons/io";
type Props = {
    onPageChange: (numb: number) => void,
    totalCount: number,
    siblingCount: number,
    currentPage : number,
    pageSize: number 
}
const Pagination = ({
    onPageChange,
    totalCount,
    siblingCount = 1,
    currentPage,
    pageSize,
}:Props) => {
    const paginationRange = usePagination({
        currentPage,
        totalCount,
        siblingCount,
        pageSize
    })
    if(paginationRange !== undefined){
        if(currentPage == 0 || paginationRange?.length < 2){
            return null
        }
    }
    const onNext = () =>{onPageChange(currentPage + 1)}
    const onPrev = () =>{onPageChange(currentPage - 1)}

    if(paginationRange == undefined)return;

    let lastPage = paginationRange[paginationRange?.length-1]

  return (
    <div className='flex gap-2 self-center'>
        <button className='' disabled={currentPage == 1} onClick={onPrev}><IoIosArrowBack/></button>
        {
            paginationRange.map((pageNumber, i) =>{
                if(pageNumber == '...'){
                    return <span key={i}>...</span>
                }
                return (
                    <button key={i} className={pageNumber == currentPage ? `bg-slate-100 p-1 w-8 h-8` :"w-8 h-8"} onClick={() => onPageChange(pageNumber as number)}>{pageNumber}</button>
                )
            })
        }
        <button onClick={onNext} disabled={currentPage == lastPage}><IoIosArrowForward/></button>
    </div>
  )
}

export default Pagination