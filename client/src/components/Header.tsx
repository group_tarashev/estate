import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { FaSearch } from "react-icons/fa";
import { Link, useNavigate } from "react-router-dom";
import { useAppContext } from "../context/AppContext";
import { useMutation } from "react-query";
import * as apiClient from '../apiClient'
type FormSearchType = {
  search: string;
};
const Header = () => {
  const [name,setName] = useState('');
  const navigate = useNavigate();
  const {mutateAsync} = useMutation(apiClient.getSearch, {onSuccess:(estates) =>{
    navigate('/search', {state:estates})
  }})
  const handleSearch =  (e:React.ChangeEvent<HTMLInputElement>) =>{
    e.preventDefault();
    setName(e.target.value)
  }
  const handleSearchSubimt = () =>{
    mutateAsync(name)
  }
  const {auth} = useAppContext();
  const { register } = useForm<FormSearchType>();
  return (
    <header className="bg-[#244855] shadow-md fixed z-10 w-full">
      <div className="flex justify-between items-center max-w-6xl mx-auto p-3 sm:gap-5 sm:justify-evenly gap-1">
        <h1 onClick={() => navigate('/home')} className="font-bold text-sm sm:text-xl flex flex-wrap cursor-pointer">
          <span className="text-white  text-xl sm:text-sm sm:hidden">No</span>
          <span className="text-white  text-xl sm:text-sm sm:hidden">Agency Estate</span>
          <span className="hidden sm:flex text-sm text-white ">NAE</span>
        </h1>
        <div className="flex items-center relative">
          <input
            type="text"
            placeholder="Tърси по град ..."
            {...register("search")}
            className="border rounded p-1 focus:outline-none sm:hidden"
            onChange={e => handleSearch(e)}
            onKeyDown={e => e.key == "Enter" && handleSearchSubimt()}
          />
          <FaSearch className="absolute right-2 cursor-pointer sm:left-2 bg-white rounded-full p-1" size={25} onClick={handleSearchSubimt} />
        </div>
        <ul className="flex gap-3 items-center">
          <Link to={'/'} className="md:hidden">
            <li className=" sm:inline text-white hover:underline">
              Начало
            </li>
          </Link>
          {auth?.username && <Link to={'/messages'} className="">
            <li className="sm:inline text-white hover:underline">
              Chat
            </li>
          </Link>}
          { auth?.username ? <Link to={'/profile'} 
          className="text-green-300">
            <div className="flex gap-1 items-center">
              <img src={auth?.avatar.toString()} className="w-7 h-7 object-cover rounded-full"  />
              <span>

            {auth.username}
              </span>
            </div>
            </Link> :<Link to={'/sign-up'}>
            <li className="px-2 bg-[#FBE9D0] rounded-lg text-black py-1" >Впиши се</li>
          </Link>}
        </ul>
      </div>
    </header>
  );
};

export default Header;
