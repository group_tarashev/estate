import  { useEffect, useState } from "react";
import useGetMessages from "../../hooks/useGetMessages";
import { useContact } from "../../context/ConversationConvext";
import Message, { MessageType } from "./Message";
const Messages = () => {
  const { selectContact, messages,setMessages } = useContact();
  const [sorted, setSorted]  = useState<MessageType[]>([]);
  useGetMessages(selectContact?._id as string);
  useEffect(() =>{
    const getSorted = messages.sort((a: MessageType, b: MessageType) => {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
    })
      setSorted(getSorted.slice(Math.max( getSorted.length - 20, 0 )))
  },[messages, setMessages])
  
  return (
    <div className="w-full flex flex-col gap-2 p-2 pb-10">
      {sorted.map((data) => (
        <Message message={data} key={data._id} />
      ))}
    </div>
  );
};

export default Messages;
