import MessageBlock from './MessageBlock'
import InputMessages from './InputMessages'
import { useContact } from '../../context/ConversationConvext';
import NoChatSelected from './NoChatSelected';

const MessageContainer = () => {
  const { selectContact } = useContact();

  if (!selectContact) {
    return <NoChatSelected />;
  }
  return (
    <div className='flex flex-col p-1 gap-4 items-start w-[500px] md:w-full justify-between  border-2 '>
      <MessageBlock/>
      <InputMessages/>
    </div>
  )
}

export default MessageContainer