import { useContact } from '../../context/ConversationConvext';
import { useSocket } from '../../context/SocketContext'

export type Contact = {
    _id: string,
    username: string,
    avatar: string,
    email:string,
    savedEstate: [],
    tel:string,
}
const Contacts = ({contact, setOpen}:{contact: Contact, setOpen:(key:boolean) => void}) => {
    const {onlineUsers} = useSocket();
    const {setSelectContact, selectContact} = useContact()
    const id :string = contact._id;
    const isOnline = onlineUsers.some( idx => idx.toString() == id) ;
    const selected = contact._id === selectContact?._id
  return (
    <div className={`flex gap-2 items-center cursor-pointer p-2 ${selected ? 'bg-white' : ''}`} onClick={() => {setSelectContact(contact); setOpen(false)}}>
        <div className='relative'>
            <img src={contact.avatar} className='avatar rounded-full w-12 h-12' alt="" />
            {isOnline &&
                <span className='w-3 h-3 absolute border-white border-2 bg-green-600 rounded-full right-0'></span>}
        </div>
        <div>
            { (<span>{contact.username}</span>)}
        </div>

    </div>
  )
}

export default Contacts