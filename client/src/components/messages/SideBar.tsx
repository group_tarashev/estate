import Contacts from "./Contacts";
import useGetConversation from "../../hooks/useGetConversation";
import { useState } from "react";


const SideBar = () => {
  const [open, setOpen] = useState(false)
  const {mutation, users} = useGetConversation();  
  if (mutation.isLoading) {
    return <span>Loading</span>;
  }
  return (
  <div className={ open ? "flex flex-col md:w-full h-screen gap-5 xl:w-[300px] bg-blue-200 p-2 relative overflow-auto z-0" 
  :
  "flex flex-col md:w-full md:h-[200px] gap-5  bg-blue-200 p-2 relative overflow-auto z-0" }>
    <button className={"font-bold bg-red-500 p-1 justify-center top-0 sm:flex md:flex xl:hidden"} onClick={() => setOpen(!open)}>{open ? "X" : "Отвори"}</button>
    <h1 className="text-2xl">Контакти:</h1>
    {users?.map((item :any, ind) =>{
      if(item._id as string == "661971dcc134fe55619f7717"){
        return <Contacts  contact={item} key={ind} setOpen={setOpen} />
      }
    })}
    {users?.map((item:any, ind) =>{
       if(item._id as string !== "661971dcc134fe55619f7717"){
        return <Contacts  contact={item} key={ind} setOpen={setOpen}/>
      }
    })}
    </div>)
};

export default SideBar;
