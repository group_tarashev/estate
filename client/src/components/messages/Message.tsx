import { useContact } from "../../context/ConversationConvext";
import { useAppContext } from "../../context/AppContext";
import useGetConversation from "../../hooks/useGetConversation";
import useListenMessages from "../../hooks/useListenMessages";
import useScorllIntoView from "../../hooks/useScorllIntoView";
import { useNavigate } from "react-router-dom";
import useGetMessages from "../../hooks/useGetMessages";

export type MessageType = {
  createdAt: Date;
  message: string;
  _id: string;
  senderId: string;
  receiverId: string;
  updatedAt: string;
};

const Message = ({ message }: { message: MessageType }) => {
  const { auth } = useAppContext();
  const { users, mutation } = useGetConversation();
  const {selectContact} = useContact();
  const {lsmsg} = useScorllIntoView();
  const navigate = useNavigate();
  const isMe = message.senderId == auth?.userId;
  useListenMessages();
  useGetMessages(selectContact?._id as string)
  const currentSender = users.filter(
    (item: any) => item._id == message.senderId
  )[0];
  // console.log(currentSender);
  if(mutation.isLoading){
    return;
  }
  return (
    <div  ref={lsmsg} className={isMe ? "flex self-end items-center" : "flex self-start items-center"} >
      {isMe || (
        <div className="p-2 cursor-pointer" onClick={() => navigate(`/user/${currentSender._id}`, {state:{currentSender}})}>
          <img
            src={currentSender?.avatar as string}
            className="w-10 h-10 rounded-full"
          />
        </div>
      )}
      <div className="p-2 flex flex-col">
        <span>{isMe || currentSender?.username}</span>
        <span className="bg-slate-200 p-2 rounded-md">{message.message}</span>
        <span className="text-sm">{formated(message.createdAt)}</span>
      </div>
    </div>
  );
};

const formated = (t:Date) =>{
  const time = new Date(t)
  const hour = time.getHours();
  const minutes = time.getMinutes();
  const form = hour + ":" + minutes
  return form
}


export default Message;
