import { useMutation } from "react-query"
import * as apiClient from '../../apiClient'
import React, { useState } from "react";
import { useContact } from "../../context/ConversationConvext";
import useListenMessages from "../../hooks/useListenMessages";
const InputMessages = () => {
  const [input,setInput] = useState("");
  const {setMessages, messages} = useContact();
  const {mutateAsync} = useMutation(apiClient.sendMesage, {onSuccess:(data) =>{
    setMessages([...messages, data])
  }});
  const  {selectContact} = useContact();
  useListenMessages();
  const handleSubmit = (e: React.FormEvent) =>{
    e.preventDefault();
    mutateAsync({receiverId: selectContact?._id as string, message: input})
    setInput('')
  }
  return (
    <form className='pb-10 flex gap-2 w-full' onSubmit={handleSubmit}>
        <input type="text" value={input} onChange={(e) => setInput(e.target.value)} className="border-2 w-full rounded-md p-2" placeholder="Пиши тук ..." />
        <button type="submit" className="bg-green-200 p-2 rounded-md">Send</button>
    </form>
  )
}

export default InputMessages