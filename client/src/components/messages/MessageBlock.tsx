import { useContact } from "../../context/ConversationConvext";
import Messages from "./Messages";
import NoChatSelected from "./NoChatSelected";

const MessageBlock = () => {
  const { selectContact } = useContact();

  if (!selectContact) {
    return <NoChatSelected />;
  }
  return (
    <div className="flex flex-col w-full h-[600px] md:h-screen relative bg-[#90AEAD] overflow-auto ">
      <h2 className="text-2xl bg-slate-500 p-1 text-white w-full  self-center">
        <span>до: </span>
        {selectContact.username}
      </h2>
      <div>
        <Messages/>
      </div>
    </div>
  );
};

export default MessageBlock;
