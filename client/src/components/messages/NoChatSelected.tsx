
const NoChatSelected = () => {
  return (
    <div className="w-full h-full md:h-[300px] flex justify-center items-center text-center bg-slate-400">
        <p className="text-2xl text-white">Изберете чат за да започнете разговор</p>
    </div>
  )
}

export default NoChatSelected