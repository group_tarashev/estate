import { MdClose } from "react-icons/md"

type Props = {
    open: boolean,
    close : () => void,
    children:React.ReactNode
}
const Modal = ({open, close, children} : Props) => {
    if(!open){
        return
    }else{
      
    }
  return (
    <div className="fixed inset-0 flex justify-center items-center z-20   bg-black bg-opacity-80">
        {children}
        <button className="absolute top-10 right-10 sm:top-4 sm:right-4 bg-opacity-50 bg-[#E64833] rounded-full" onClick={close}> <MdClose color="white" size={30}/></button>
    </div>
  )
}

export default Modal