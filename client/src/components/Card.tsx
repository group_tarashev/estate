import { useNavigate } from "react-router-dom";
import { FiNavigation } from "react-icons/fi";
import { EstateType } from "./ManageForm";

type Props = {
  estate: EstateType;
};
const Card = ({ estate }: Props) => {
  const navigate = useNavigate();
  // console.log()
  return (
    <div className="w-[270px] border rounded-lg overflow-hidden flex flex-col h-[440px] shadow-md bg-white">
      <img
        className="cursor-pointer min-h-[200px] object-cover"
        src={estate.imagesUrls[0] as string}
        alt=""
        onClick={() => navigate(`/estate/${estate._id}`, { state: estate })}
      />
      <h1 className="pt-5 pl-5 font-bold text-md cursor-pointer capitalize " onClick={() => navigate(`/estate/${estate._id}`, { state: estate })}>
        {estate.name.substring(0, 24)} ...
      </h1>
      <div className="p-5 flex flex-col  justify-between gap-2 h-full ">
        <span className="flex gap-2 items-center text-md capitalize">
          <FiNavigation /> {estate.address.substring(0, 20)}
        </span>
        <p className="line-camp-3 text-sm text-wrap text-slate-500">
          {estate.description.substring(0, 70)}...
        </p>
        <div>
          <div>
            <span className="text-lg text-slate-600">{estate.price}{" "}{estate.rent || estate.mate ? "лв / м" : "лв" }</span>
          </div>
          <div className="flex gap-10">
            {estate.beds && <span className="font-bold">{estate.beds} легла</span>}
            {estate.baths && <span className="font-bold">{estate.baths} бани</span>}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
