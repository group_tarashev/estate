import { useState } from "react";
import { FiArrowLeft, FiArrowRight } from "react-icons/fi";

type Props = {
  images?: string[]
}
const Images = ({images}:Props) => {
    const [imgIndex, setImgIndex] = useState(0);
    if(!images){
      return
    }
  const handleImageInc = () => {
    setImgIndex((prev) =>
      prev > images.length - 2 ? 0 : prev + 1
    );
  };
  const handleImageDcs = () => {
    setImgIndex((prev) =>
      prev <= 0 ? images.length - 1 : prev - 1
    );
  };
  return (
    <div className="flex relative">
    <button className="absolute left-4 top-1/2" onClick={handleImageDcs}>
      <FiArrowLeft
        size={36}
        color="white"
        className="rounded-full bg-slate-800 p-1"
      />
    </button>
    {images.map((image, i) => (
      <img
        className={
          imgIndex == i ? " w-full  " : "hidden"
        }
        src={image.toString()}
        key={i}
        alt=""
      />
    ))}
    <button className="absolute right-4 top-1/2" onClick={handleImageInc}>
      <FiArrowRight
        size={36}
        color="white"
        className="rounded-full bg-slate-800 p-1"
      />
    </button>
  </div>
  )
}

export default Images