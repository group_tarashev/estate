import { Link } from 'react-router-dom';

const Footer = () => {
    const date = new Date().getFullYear();
  return (
    <div className='h-12 w-full md:w-screen bg-blue-900 flex flex-col justify-center p-2 items-center self-end'>
        <ul>
            <Link to={'/rules'}>
                <p className='text-white hover:underline'>Правила и условия за ползване</p>
            </Link>
        </ul>
        <p className='text-xl text-white'>Copyright <span>{date}</span></p>
    </div>
  )
}

export default Footer