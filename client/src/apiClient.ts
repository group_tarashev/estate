import { GoogleType } from "./components/OAuth";
import { LoginForm } from "./pages/Signin";
import { RegisterForm } from "./pages/Signup"
const url = import.meta.env.VITE_APP_BASE_URL || ""
export const registration = async (formData: RegisterForm) =>{
    const response = await fetch(`${url}/api/signup`,{
        method: "POST",
        credentials: "include",
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formData)
    })
    if(!response.ok){
        throw new Error("Error with registration from apiClient")
    }
    return response.json();
}

export const login = async (formData: LoginForm) =>{
    const response = await fetch(`${url}/api/signin`, {
        method:"POST",
        credentials:"include",
        headers:{
            "Content-Type": "application/json"
        },
        body:JSON.stringify(formData)
    })
    if(!response.ok){
        console.log(response.statusText)
        throw new Error("Error with login " + response.statusText)
    }
    return response.json();
}

export const signinGoogle = async (formData: GoogleType) =>{    
    const response = await fetch(`${url}/api/google`,{
        method: "POST",
        credentials:"include",
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formData)
    })
    if(!response.ok){
        throw new Error("Error sign in with google")
    }
    return response.json();
}

export const updateProf =async (formData:FormData) =>{
    // console.log(formData)
    const response = await fetch(`${url}/api/update-prof`,{
        method:"POST",
        credentials:"include",
        body:formData
    })
    if(!response.ok){
        throw new Error("Error updated profile")
    }
    return response.json();
}

export const signOut = async () =>{
    const response = await fetch(`${url}/api/sign-out`,{method:"POST", credentials:'include'})
    if(!response.ok){
        throw new Error("Could not sign out")
    }
}
export const addEstate = async (formData:FormData) =>{
    // console.log(formData)
    const response = await fetch(`${url}/api/add-estate`,{
        method:"POST",
        credentials:'include',
        body: formData
    })
    if(!response.ok){
        throw new Error("Problem with saving estate")
    }
    response.json();
}
export const getEstates = async()=>{
    const response = await fetch(`${url}/api/get-estates`)
    return response.json();
}

export const getUserEstate = async() =>{
    const response = await fetch(`${url}/api/get-user-estate`,{
        credentials:'include'
    })
    return response.json();
}
export const getUserEstateById = async(id:string) =>{
    const res = await fetch(`${url}/api/get-user-estate-by-id/${id}`)
    if(!res.ok){
        throw new Error ( "Error get estate by user")
    }
    return res.json();
}
export const updateEstateById = async(formData:FormData) =>{
    const response = await fetch(`${url}/api/update-state-by-id/`,{
        method:"PUT",
        // headers:{
        //     "Content-Type":'application/json'
        // },
        credentials:"include",
        body: formData
    })
    if(!response.ok){
        throw new Error("Connot get estate by id")
    }
    return response.json();
}
export const deleteEstate = async(id: string) => {
    const response = await fetch(`${url}/api/delete-estate/${id}`,{
        method:"DELETE",
        credentials:'include'
    })
    if(!response.ok){
        throw new Error("Error with deleting estate")
    }
} 

export const safeEstate = async(id:string) =>{
    const response = await fetch(`${url}/api/safe-estate/${id}`,{
        method: "POST",
        credentials:'include',
    })
    if(!response.ok){
        throw new Error("Error with saving estate")
    }
}

export const getSaved = async() =>{
    const response = await fetch(`${url}/api/saved-estate`,{
        credentials:'include'
    })
    if(!response.ok){
        throw new Error("Error get saved estates")
    }
    return response.json();
}
export const removeSaved = async (id:string) =>{
    const response = await fetch(`${url}/api/remove-safed/${id}`,{
        method: "DELETE",
        credentials:'include',
    })
    if(!response.ok){
        throw new Error("Error with removing saved estate")
    }
    return response.json();
}
export const getSearch = async (search:string) =>{
    const response = await fetch(`${url}/api/get-search?search=${search}`)
    if(!response.ok){
        throw new Error('Error with search query')
    }
    return response.json();
}
type FormSearch = {
    address: string,
    rent: boolean,
    sell:boolean,
    mate: boolean,
    parking: boolean,
    offer: boolean,
    sort: number,
    type:string,
    furnish: boolean
}
export const getSearchOptions = async (formData:FormSearch) =>{
    const formUrl = new URLSearchParams();
    formUrl.append('address', formData.address);
    formUrl.append('type', formData.type);
    formUrl.append('rent', formData.rent.toString());
    formUrl.append('sell', formData.sell.toString());
    formUrl.append('furnish', formData.furnish.toString());
    formUrl.append('mate', formData.mate.toString());
    formUrl.append('parking', formData.parking.toString());
    formUrl.append('offer', formData.offer.toString());
    formUrl.append('sort', formData.sort.toString());


    const response = await fetch(`${url}/api/get-search-option?${formUrl}`)
    if(!response.ok){
        throw new Error('Error with search option')
    }
    return response.json();
}

export const fetcSearchData = async (setAllCities : (all: string[]) => void) => {
    try {
      const res = await fetch("../src/assets/cities.json");
      const js:{country: string[]} = await res.json();
      const all = Object.values(js).reduce((acc: string[], cities: string[]) => [...acc, ...cities], []);
      setAllCities(all);
      // console.log(all)
    } catch (error) {
      console.log(error);
    }
  };

  export const getMessages = async(userId:string) =>{
    const res = await fetch(`${url}/api/get-messages/${userId}`,{
        credentials:'include'
    });
    if(!res.ok){
        throw new Error("Messages not come")
    }
    return res.json()
  }

  export const getMainChats = async(mainId:string) =>{
    const res=  await fetch(`${url}/api/get-main-chat/${mainId}`,{
        credentials: 'include'
    })
    if(!res.ok){
        throw new Error("Meesage error")
    }
    return res.json();
  }

  export const getUsers = async() =>{
    const res = await fetch(`${url}/api/get-users`,{
        credentials:'include'
    });
    if(!res.ok){
        throw new Error("User not received")
    }
    return res.json();
  }

  export  const sendMesage = async({receiverId, message}:{receiverId:string, message:string}) =>{
    const res = await fetch(`${url}/api/send-message/${receiverId}`,{
        method:"POST",
        credentials:'include',
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify({message})
    })
    if(!res.ok){
        throw new Error("Error send message")
    }
    return res.json();
  }