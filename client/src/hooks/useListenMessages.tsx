import  { useEffect } from "react";
import { useSocket } from "../context/SocketContext";
import { useContact } from "../context/ConversationConvext";

const useListenMessages = () => {
  const { socket } = useSocket();
  const { setMessages, messages } = useContact();

  useEffect(() => {
    const time = setTimeout(() => {
      const handleMessage = (newMessage: any) => {
        setMessages([...messages, newMessage]);
      };
      if (socket) {
        socket?.on("newMessage", handleMessage);
        return () => socket?.off("newMessage", handleMessage);
      }
    }, 1000);
    return () => clearTimeout(time);
  }, [messages, socket, setMessages]);
};

export default useListenMessages;
