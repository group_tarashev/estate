import { useEffect } from "react";
import { useContact } from "../context/ConversationConvext";
import { useMutation } from "react-query";
import * as apiClient from "../apiClient";

const useGetMessages = (userToChatId: string) => {
  const { setMessages } = useContact();

  const { mutateAsync: getMessages, isLoading } = useMutation(
    apiClient.getMessages,
    {
      onSuccess: (data) => {
        setMessages(data);
      },
    }
  );

  const { mutateAsync: getMainChats, isLoading: mainChatsLoading } =
    useMutation(apiClient.getMainChats, {
      onSuccess: (data) => {
        setMessages(data);
      },
    });

  useEffect(() => {

    const fetchData = async () => {
      try {
        if (userToChatId === "661971dcc134fe55619f7717") {
          await getMainChats(userToChatId);
        } else {
          await getMessages(userToChatId);
        }
      } catch (error) {
        console.error("Error fetching messages:", error);
      }
    };

    fetchData();

  }, [userToChatId, getMessages, getMainChats]);

  return { isLoading: isLoading || mainChatsLoading };
};

export default useGetMessages;
