import { useEffect, useState } from "react";
import { useMutation } from "react-query";
import * as apiClient from "../apiClient";

export type UserType ={
  _id:string,
  avatar:string,
  email:string,
  username:string,
  tel:string,
  savedEstate:[]
}

const useGetConversation = () => {
  const [users, setUsers] = useState<Array<UserType>>([]);
  const mutation = useMutation(apiClient.getUsers, {
    onSuccess: (data) => {
      setUsers(data);
    },
  });

  useEffect(() => {
    mutation.mutateAsync();
  }, []);

  return { mutation, users };
};

export default useGetConversation;
