import  { useMemo } from 'react'

type Props = {
    totalCount: number,
    pageSize: number,
    siblingCount: number,
    currentPage: number
}
const range = (start:number, end:number) =>{
    let length = end - start +1
    return Array.from({length}, (__,idx) => idx + start)
}
const usePagination = ({
    totalCount,
    pageSize,
    siblingCount = 1,
    currentPage
}:Props) => {
    const paginationRange = useMemo(() =>{
        const totalPageCount = Math.ceil( totalCount/  pageSize);
        const totalPageNumber=  siblingCount + 5;


        if(totalPageCount <= totalPageNumber){
            return range(1, totalPageCount)
        }
        const leftSiblingIndex = Math.max(currentPage - siblingCount, 1);
        const rightSiblingIndex = Math.min(currentPage + siblingCount, totalPageCount)

        const shouldShowLeftDot = leftSiblingIndex > 2;
        const shouldShowRightDot = rightSiblingIndex < totalPageCount - 2

        const firstPageIndex = 1;
        const lastPageIndex = totalPageCount

        //no left dots to show but show right
        if(!shouldShowLeftDot && shouldShowRightDot){
            let leftItemCount = 3 + 2 * siblingCount
            let leftRange = range(1, leftItemCount)
            return [...leftRange, '...', totalPageCount]
        }
        //show left dots dont show right dots
        if(shouldShowLeftDot && !shouldShowRightDot){
            let rigthItemCount = 3 + 2 * siblingCount;
            let rightRange = range(totalPageCount -rigthItemCount + 1, totalPageCount)
            return [firstPageIndex, "...", ...rightRange]
        }

        //show left and right dots 
        if(shouldShowLeftDot && shouldShowRightDot){
            let middRange = range(leftSiblingIndex,rightSiblingIndex)
            return [firstPageIndex,"...", ...middRange,"...", lastPageIndex]
        }

    },[totalCount, pageSize, currentPage, siblingCount])
    return paginationRange
}

export default usePagination