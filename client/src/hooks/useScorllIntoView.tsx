import { useEffect, useRef } from 'react'
import { useContact } from '../context/ConversationConvext'
import { MessageType } from '../components/messages/Message';

const useScorllIntoView = () => {
  const {messages,setMessages} = useContact();
  const lsmsg = useRef<null | HTMLDivElement>(null)
  const prev  = useRef<MessageType[]>([]);
  useEffect(() =>{
    const isNewMessage = messages.length > prev.current.length
    if(isNewMessage){
      lsmsg.current?.scrollIntoView({behavior:'smooth', block:'center'})
    }
    prev.current = messages;
  },[messages, setMessages])
  return {lsmsg}
}
export default useScorllIntoView