import { createContext, useContext, useEffect, useState } from 'react'
import { useCookies } from 'react-cookie'
import io, {Socket} from  'socket.io-client'


type SocketI = {
    onlineUsers: Array<object>,
    socket: Socket | null
}
const SocketContext = createContext<SocketI | null>(null);

export const SocketContextProvider = ({children} :{children:React.ReactNode}) =>{
    const [cookies] = useCookies(['user']);
    const [onlineUsers, setOnlineUsers] = useState([]);
    const [socket, setSocket] = useState<Socket | null>(null)

    useEffect(()=>{
        if(cookies.user){
            const socket = io('http://localhost:3001', {
                query:{
                    userId: cookies.user.userId
                }
            })
            setSocket(socket)
            socket.on('getOnlineUsers', (users) =>{
                setOnlineUsers(users);
                // console.log(users)
            })
            return () => { socket.close()};
        }else{
            if(socket){
                // socket.close();
                setSocket(null);
            }
            
        }
        
    },[cookies.user])


    return <SocketContext.Provider value={{socket, onlineUsers}}>{children}</SocketContext.Provider>
}

export const useSocket = () =>{
    return useContext(SocketContext) as SocketI
}