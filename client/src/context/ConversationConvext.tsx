import { createContext, useContext, useState } from "react";
import { MessageType } from "../components/messages/Message";

export type Contact = {
  _id: string;
  username: string;
  avatar: string;
  email: string;
  savedEstate: [];
  tel: string;
};

type ContaxtContext = {
  selectContact: Contact | null;
  setSelectContact: (selectContact: Contact) => void;
  messages: Array<MessageType>;
  setMessages: (messages: Array<MessageType>) => void;
};

const ConversationContext = createContext<ContaxtContext | null>(null);

export const ConversationConvextProvider = ({
  children,
}: {
  children: React.ReactNode;
}) => {
  const [messages, setMessages] = useState<Array<MessageType> | []>([]);
  const [selectContact, setSelectContact] = useState<Contact | null>(null);
  return (
    <ConversationContext.Provider
      value={{
        messages,
        setMessages,
        selectContact,
        setSelectContact,
      }}
    >
      {children}
    </ConversationContext.Provider>
  );
};

export default ConversationConvextProvider;

export const useContact = () => {
  return useContext(ConversationContext) as ContaxtContext;
};
