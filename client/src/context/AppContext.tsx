import { createContext, useContext,  useState } from "react"
import {useCookies} from 'react-cookie'
type AppContext = {
    username: string,
    email: string,
    userId: string,
    avatar: string,
    tel:string,
    savedEstates: Object[]   
}

type AppContextType = {
    onLogin :  (data: AppContext | undefined) => void
    auth: AppContext | undefined
}

const AppContext = createContext<AppContextType | undefined>(undefined)

type Props = {
    children: React.ReactNode
}

const AppContextProvider = ({children} :Props) =>{
    const [cookies] = useCookies(['user'])
    const initialUser = cookies.user ? cookies.user : undefined
    const [auth, setAuth] = useState<AppContext | undefined>(initialUser);
    
    const onLogin = (data: AppContext | undefined) =>{
         setAuth(data)
        //  console.log(data)
        //  setCookies('user', JSON.stringify(data),{path:'/', maxAge:864000})
    }   
    
    return <AppContext.Provider value={{onLogin, auth}}>{children}</AppContext.Provider>
} 

export const useAppContext = () =>{
    const context = useContext(AppContext);
    return context as AppContextType
}

export default AppContextProvider