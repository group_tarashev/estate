import {create} from 'zustand'
import { Contact } from '../components/messages/Contacts'

const useConversation = create((set) =>({
    selectConversation: null,
    setSelectConversation: (selectConversation : Contact) => set({selectConversation}),
    messages: [],
    setMessages: (messages :[]) => set({messages})
}))

export default useConversation