import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Signin from "./pages/Signin";
import Signup from "./pages/Signup";
import About from "./pages/About";
import Profile from "./pages/Profile";
import Header from "./components/Header";
import { useAppContext } from "./context/AppContext";
import AddEstate from "./pages/AddEstate";
import Estate from "./pages/Estate";
import EditEstate from "./pages/EditEstate";
import SearchPage from "./pages/SearchPage";
import Messages from "./pages/Messages";
import UserProfile from "./pages/UserProfile";
import Footer from "./components/Footer";
import Rules from "./pages/Rules";

const App = () => {
  const { auth } = useAppContext();
  return (
    <div className="flex flex-col justify-between h-full md:h-full w-full ">
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/sign-in" element={<Signin />} />
        <Route path="/sign-up" element={<Signup />} />
        <Route path="/about" element={<About />} />
        <Route path="/add-estate" element={<AddEstate />} />
        <Route path="/estate/:id" element={<Estate />} />
        <Route path="/edit-estate/:id" element={<EditEstate />} />
        <Route path="/search" element={<SearchPage />} />
        {auth?.username && <Route path="/user/:id" element={<UserProfile />} />}
        {auth?.username && <Route path="/profile" element={<Profile />} />}
        {auth?.username && <Route path="/messages" element={<Messages />} />}
        <Route path="*" element={<Home />} />
        <Route path="/rules" element={<Rules/>}/>
      </Routes>
      <Footer/>
    </div>
  );
};

export default App;
