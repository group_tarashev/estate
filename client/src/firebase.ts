import { initializeApp } from "firebase/app";

// TODO: Add SDKs for Firebase products that you want to use

// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration

const firebaseConfig = {
  apiKey: import.meta.env.VITE_APP_FIREBASE_KEY,

  authDomain: "estate-mer.firebaseapp.com",

  projectId: "estate-mer",

  storageBucket: "estate-mer.appspot.com",

  messagingSenderId: "37890227082",

  appId: "1:37890227082:web:084a8de0ad9e6fc106e673",
};

// Initialize Firebase

export const app = initializeApp(firebaseConfig);
