import express, { Request, Response } from 'express'
import cors from 'cors'
import mongoose from 'mongoose'
import cookieParser from 'cookie-parser'
import 'dotenv/config'
import userRoute from './routes/users'
import {v2 as cloudinary} from 'cloudinary';
import estateRoute from './routes/estateRoute'
import messageRoute from './routes/messages'
import path from 'path'
import {app, server} from './socket/socket'
const PORT = process.env.PORT || 3001 
const url = process.env.FRONT_URL 
cloudinary.config({
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.CLOUD_API_KEY,
    api_secret: process.env.CLOUD_API_SECRET
})
console.log(url)
app.use(express.urlencoded({extended: true}))
app.use(express.json());
app.use(express.static(path.join(__dirname,  '../../client/dist') ))
app.use(cors({
    origin:["http://localhost:5173", url],
    credentials: true
}));
app.use(cookieParser());

app.use('/api/test', (req:Request, res:Response) =>{
    res.send("Test Api")
})

app.use('/api', userRoute);
app.use('/api', estateRoute)
app.use('/api', messageRoute);

mongoose.connect(process.env.MONGO_ENV as string).then(()=>{
    console.log("Connected to DB")
}) 
server.listen(PORT, () =>{
    console.log("Server running on port 3001")
})

// console.log(
//     Math.random().toString(36).slice(-8) + Math.random().toString(36).slice(-8)
// )

