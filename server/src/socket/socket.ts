import {Server} from 'socket.io'
import express from 'express'
import http from 'http'
const app = express();
const server = http.createServer(app)
const url = process.env.FRONT_URL 
const io = new Server(server,{
    cors:{
        origin:[url, 'http://localhost:5173'],
        methods:['POST',"GET"]
    }
});

export const getReceiverSocektId = (receiverId:string) =>{
    return userSocketMap[receiverId]
}

let userSocketMap:{[key:string] :any} = {};
io.on('connection', (socket) =>{
    // console.log("socket id: ",socket.id);
    const userId = socket.handshake.query.userId
    // console.log("user", socket.handshake.query.userId)
    if(userId !== 'undefined')userSocketMap[userId as string] = socket.id

    io.emit('getOnlineUsers', Object.keys(userSocketMap))

    socket.on('disconnect', () =>{
        // console.log("disconnect", socket.id)
        delete userSocketMap[userId as string]
        io.emit('getOnlineUsers', Object.keys(userSocketMap))
    })
})

export {app, io, server}