import{v2 as cloudinary} from 'cloudinary'

export const uplaodImage =async (image: Express.Multer.File[]) =>{
    const uplaodPromise = image.map(async(img) =>{
      const bs64 = Buffer.from(img.buffer).toString("base64");
      let dataUrl = "data:" + img.mimetype + ";base64," + bs64
      const res = await cloudinary.uploader.upload(dataUrl)
      return res.url
    })
    const imageUrl = await Promise.all(uplaodPromise)
    return imageUrl
  }