import { NextFunction, Request, Response } from "express";
import jwt, { JsonWebTokenError } from 'jsonwebtoken'
declare global {
    namespace Express{
        interface Request{
            userId: String
        }
    }
}
export interface TokenInterface {
    userId: string;
    username: string;
  }
export const verifyToken = (req:Request, res:Response, next:NextFunction) =>{
    const token = req.cookies._au_tkn;
    if(!token){
        return res.status(401).json({message: "Unauthorized"})
    }

    try {
        const decoded = jwt.verify(token, process.env.JWT_KEY as string) as TokenInterface;
        req.userId = decoded.userId;
        next();
    } catch (error) {
        if (error instanceof JsonWebTokenError) {
            console.error("JWT verification error:", error);
            return res.status(401).json({ message: "Unauthorized" });
        } else {
            console.error("Unexpected error:", error);
            return res.status(500).json({ message: "Internal server error" });
        }
    }
}