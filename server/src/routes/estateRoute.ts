import express from 'express'
import { verifyToken } from '../middleware/veryfytoken';
import { addEstate, deleteEstate, getEstates, getSavedEstate, getSearch, getSearchOption, getUserEstate,getUserEstateById,removeSaved,updateEstateById } from '../controller/estateController';
import {upload} from '../middleware/mutler'
const router = express.Router();

router.post('/add-estate', verifyToken, upload.array("imagesFiles", 6), addEstate )
router.get('/get-estates', getEstates)
router.get('/get-user-estate', verifyToken, getUserEstate)
router.get('/get-user-estate-by-id/:id', getUserEstateById)
router.put('/update-state-by-id', upload.array('imagesFiles', 6), verifyToken, updateEstateById);
router.get('/get-search', getSearch)
router.get('/get-search-option',getSearchOption)
router.delete('/delete-estate/:id',verifyToken, deleteEstate);
router.get('/saved-estate', verifyToken, getSavedEstate);
router.delete('/remove-safed/:id', verifyToken, removeSaved)
export default router