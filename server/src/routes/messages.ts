import express from 'express'
import { getMainChatMessage, getMessage, sendMessage } from '../controller/messages';
import { verifyToken } from '../middleware/veryfytoken';

const router = express.Router();

router.post('/send-message/:id',verifyToken, sendMessage); 
router.get('/get-messages/:id',verifyToken, getMessage)
router.get('/get-main-chat/:id', getMainChatMessage)
export default router

