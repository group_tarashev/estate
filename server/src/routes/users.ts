import express, { Request, Response } from 'express'
import { getUsers, google, safeEstate, signin, signup, updateProf } from '../controller/users';
import { verifyToken } from '../middleware/veryfytoken';
import {upload} from '../middleware/mutler'
const router = express.Router();



router.post('/signup', signup);
router.post('/signin', signin);
router.post('/google', google);
router.post('/update-prof', verifyToken, upload.array('image', 1), updateProf)
router.post('/sign-out', (req :Request, res:Response) =>{
    // req.cookies.auth_token = '';
    return res.clearCookie('_au_tkn').clearCookie('user').status(200).json({message: "Succesful sign out"})
})
router.post('/safe-estate/:id', verifyToken, safeEstate);
router.get('/get-users', verifyToken, getUsers)
export default router