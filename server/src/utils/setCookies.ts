export const setCookiesOptions = (secure: string) => {
  return {
    httpOnly: true,
    secure: secure,
    sameSite: "none",
    path: "/",
    maxAge: 1000 * 60 * 60 * 24,
  };
};
