import { Response } from "express";
import mongoose from "mongoose";
type User = {
  username: string;
  userId: mongoose.Types.ObjectId;
  email: string;
  avatar: string;
  tel: string | null;
};
export const setCookieToken = (res: Response, token: string, name: string) => {
  res.cookie(name, token, {
    httpOnly: false,
    // secure: !isLocal,
    path: "/",
    sameSite: "strict",
    maxAge: 1000 * 60 * 60 * 24,
  });
};

export const setCookieUser = (res: Response, user: User, name: string) => {
  res.cookie("user", JSON.stringify(user), {
    httpOnly: false,
    // secure: !isLocal,
    path: "/",
    sameSite: "strict",
    maxAge: 1000 * 60 * 60 * 24,
  });
};
