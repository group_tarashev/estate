import mongoose from "mongoose";
import UserEstate from "./user";

type Message = {
  sender: mongoose.Schema.Types.ObjectId;
  recipient: mongoose.Schema.Types.ObjectId;
  text: string;
};

const messageShema = new mongoose.Schema(
  {
    senderId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: UserEstate,
      required: true,
    },
    receiverId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: UserEstate,
      required: true,
    },
    message: { type: String, required: true },
  },
  { timestamps: true, expireAfterSeconds: 60 * 60 * 24 }
);

export const Message = mongoose.model<Message>("Message", messageShema);
