import mongoose from "mongoose";

export type EstateType = {
    name: string;
    description: string;
    address: string;
    type: string;
    sell: boolean;
    rent: boolean;
    water: boolean;
    power: boolean;
    mate:boolean;
    kv: number;
    etaj: number;
    parking: boolean;
    furnished: boolean;
    offer: boolean;
    beds: number;
    baths: number;
    price: number;
    imagesUrls: String[];
    userId: string,
    userTel: string,
  };
  
  const estateSchema = new mongoose.Schema({
    name:{type:String, required: true},
    description:{type:String, required: true},
    type:{type:String, required: true},
    address:{type:String, required: true},
    kv:{type: Number},
    etaj: {type: Number},
    water:{type: Boolean},
    power:{type: Boolean},
    sell:{type:Boolean,},
    mate:{type:Boolean},
    rent:{type:Boolean, },
    parking:{type:Boolean,},
    furnished:{type:Boolean,},
    offer:{type:Boolean,},
    beds:{type:Number, },
    baths:{type:Number,},
    price:{type:Number, required: true},
    imagesUrls: {type:Array ,required: true},
    userId:{type:String, required: true },
    userTel:{type: String, required: true}
  })

  const Estate = mongoose.model<EstateType>("Estate", estateSchema);

  export default Estate;