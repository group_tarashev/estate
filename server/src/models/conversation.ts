import mongoose from "mongoose";
import UserEstate from "./user";
import { Message } from "./message";

const conversationSchema = new mongoose.Schema({
    participant:[
        {
            type:mongoose.Schema.Types.ObjectId,
            ref:UserEstate
        }
    ],
    messages:[
        {
            type:mongoose.Schema.Types.ObjectId,
            ref:Message,
            default:[]
        }
    ]
},{timestamps:true})

const Conversation = mongoose.model("Conversation", conversationSchema);

export default Conversation