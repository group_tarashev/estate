import mongoose from "mongoose"

export type UserType = {
    username: string,
    password: string,
    email: string,
    avatar: string,
    tel: string,
    savedEstate: Object[]
}

const userShema = new mongoose.Schema({
    username: {type: String, required: true},
    password: {type: String, required: true},
    email: {type:String, required: true, unique: true},
    avatar:{type: String, default: "https://media.istockphoto.com/id/1298261537/vector/blank-man-profile-head-icon-placeholder.webp?s=1024x1024&w=is&k=20&c=4ZDljeyUFFmyjlHUV0BYEMWTr8SyKQR6FMWtew14jq0="},
    tel: {type: String, unique: true},
    savedEstate: {type:Array}
})

const UserEstate = mongoose.model<UserType>("UserEstate", userShema)

export default UserEstate