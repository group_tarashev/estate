import { Request, Response } from "express";
import Conversation from "../models/conversation";
import { Message } from "../models/message";
import { getReceiverSocektId } from "../socket/socket";
import { io } from "../socket/socket";
declare global {
  namespace Express {
    interface Request {
      userId: String;
    }
  }
}
export const sendMessage = async (req: Request, res: Response) => {
  const { message } = req.body;
  const { id: receiverId } = req.params;
  const senderId = req.userId;
  // console.log(message, " ", receiverId, " ", senderId);
  try {
    let conv = await Conversation.findOne({
      participant: { $all: [receiverId, senderId] },
    });
    if (!conv) {
      conv = await Conversation.create({
        participant: [senderId, receiverId],
      });
    }
    const newMessage = await Message.create({
      senderId,
      receiverId,
      message,
    });
    if (conv) {
      conv.messages.push(newMessage._id);
    }
    await Promise.all([newMessage.save(), conv.save()]);

    const receiverIdSocket = getReceiverSocektId(receiverId)
    if(receiverIdSocket){
      io.to(receiverIdSocket).emit("newMessage", newMessage)
    }

    res.status(200).json(newMessage);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};

export const getMessage = async (req: Request, res: Response) => {
  const { id: userToChat } = req.params;
  const senderId = req.userId;
  // console.log(userToChat, ",", senderId);
  try {
    const conversation = await Conversation.findOne({
      participant: { $all: [senderId, userToChat] },
    }).populate("messages");

    if (!conversation) return res.status(200).json([]);

    return res.status(200).json(conversation?.messages);
  } catch (error) {
    return res.status(500).json({ message: "Server error" });
  }
};

export const getMainChatMessage = async (req: Request, res:Response) =>{
  const {id: mainChat} = req.params;
  // console.log(mainChat)
  try {
    const conversation = await Conversation.find({participant:{$all: [mainChat]}}).populate('messages').select('messages')
    if(!conversation)return res.status(200).json([]);
    const ms =  conversation.flatMap((item:any) =>{
      // console.log(item)
      return item.messages
      }) 
    
    return res.status(200).json(ms)
  } catch (error) {
    return res.status(500).json({ message: "Server error", error });
    
  }
}