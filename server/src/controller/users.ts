import { Request, Response } from "express";
import UserEstate from "../models/user";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import storage from "multer";
import { uplaodImage } from "../middleware/cloudinary";
import Estate from "../models/estate";
import { Message } from "../models/message";
import { setCookieToken, setCookieUser } from "../utils/cookies";
import mongoose from "mongoose";

export const signup = async (req: Request, res: Response) => {
  const { password, username, email, tel } = req.body;
  const isLocal = req.hostname == "localhost";
  try {
    let user = await UserEstate.findOne({ email });
    if (user) {
      return res.status(401).send({ message: "User Already exist" });
    }
    const salt = bcrypt.genSaltSync(6);
    const hash = bcrypt.hashSync(password, salt);
    user = new UserEstate({
      username: username,
      email: email,
      password: hash,
      tel: tel,
    });
    await user.save();
    const { password: pass, ...rest } = user.toObject();
    const token = jwt.sign(
      { userId: user._id },
      process.env.JWT_KEY as string,
      { expiresIn: "1d" }
    );
    setCookieToken(res, token, "_au_tkn");
    setCookieUser(
      res,
      {
        username: user.username,
        userId: user._id,
        email: user.email,
        avatar: user.avatar,
        tel: user.tel,
      },
      "user"
    );

    return res
      .status(200)
      .send({ message: "Successful registration", userId: user._id });
  } catch (error) {
    return res.status(500).send({ message: "Error with registration" });
  }
};

export const signin = async (req: Request, res: Response) => {
  try {
    const isLocal = req.hostname == "localhost";
    const { password, email } = req.body;
    const user = await UserEstate.findOne({ email });
    if (!user) {
      return res.status(404).send({ message: "User does not exist" });
    }
    const isMatch = bcrypt.compareSync(password, user.password);
    if (!isMatch) {
      return res.status(401).send({ message: "Password does not match" });
    }
    // console.log(email)
    const token = jwt.sign(
      { userId: user._id, username: user.username },
      process.env.JWT_KEY as string,
      { expiresIn: "1d" }
    );
    // console.log(token)
    setCookieToken(res, token, "_au_tkn");
    setCookieUser(
      res,
      {
        username: user.username,
        userId: user._id,
        email: user.email,
        avatar: user.avatar,
        tel: user.tel,
      },
      "user"
    );

    return res.status(200).send({
      message: "Successful login",
      userId: user._id,
      username: user.username,
      email: user.email,
      avatar: user.avatar,
      tel: user.tel,
    });
  } catch (error) {
    return res.status(500).send({ message: "Something went wrong with login" });
  }
};
export const google = async (req: Request, res: Response) => {
  const { username, email, avatar } = req.body;
  const isLocal = req.hostname == "localhost";

  const isExist = await UserEstate.findOne({ email });
  if (isExist) {
    const token = jwt.sign(
      { userId: isExist._id, username: isExist.username },
      process.env.JWT_KEY as string,
      { expiresIn: "1d" }
    );
    setCookieToken(res, token, "_au_tkn");
    setCookieUser(
      res,
      {
        username: isExist.username,
        userId: isExist._id,
        email: isExist.email,
        avatar: isExist.avatar,
        tel: isExist.tel,
      },
      "user"
    );

    return res.status(200).json({
      message: "Successful login",
      userId: isExist._id,
      username,
      email,
      avatar,
      tel: isExist.tel,
    });
  } else {
    const genPass =
      Math.random().toString(36).slice(-8) +
      Math.random().toString(36).slice(-8);
    const hash = bcrypt.hashSync(genPass, 10);
    const newUser = new UserEstate({ username, email, avatar, password: hash });
    await newUser.save();
    const token = jwt.sign(
      { userId: newUser._id },
      process.env.JWT_KEY as string,
      { expiresIn: "1d" }
    );
    setCookieToken(res, token, "_au_tkn");
    setCookieUser(
      res,
      {
        username: newUser.username,
        userId: newUser._id,
        email: newUser.email,
        avatar: newUser.avatar,
        tel: null,
      },
      "user"
    );
    return res.status(200).json({
      message: "Successful login",
      userId: newUser._id,
      username,
      email,
      avatar,
      tel: null,
    });
  }
};

export const updateProf = async (req: Request, res: Response) => {
  // console.log(req.body)
  try {
    const { username, email } = req.body;
    // console.log(pass)
    const image = req.files as Express.Multer.File[];
    const imageUrl = await uplaodImage(image);
    // console.log(imageUrl)
    let hash, user;

    // console.log(req.body)
    if (req.body.password) {
      hash = bcrypt.hashSync(req.body.password.toString(), 8);
      user = await UserEstate.findByIdAndUpdate(
        req.userId,
        {
          password: hash,
        },
        { new: true }
      );
    }
    user = await UserEstate.findByIdAndUpdate(
      req.userId,
      {
        username,
        email,
        // password: hash && hash,
        avatar: imageUrl[0],
        tel: req.body.tel,
      },
      { new: true }
    );
    await user.save();
    if (!user) {
      throw Error("User not found");
    }
    const { password: _, ...rest } = user?.toObject();
    setCookieUser(
      res,
      {
        username: user.username,
        userId: user._id,
        email: user.email,
        avatar: user.avatar,
        tel: user.tel,
      },
      "user"
    );
    return res.status(200).json(rest);
  } catch (error) {
    return res.status(500).json({ message: "Error update profile" });
  }
};

// ----------------- Safe Estate -----------

export const safeEstate = async (req: Request, res: Response) => {
  const estate_id = req.params.id;
  const userId = req.userId;
  // console.log(req.userId)
  try {
    const estate = await Estate.findById(estate_id);
    if (!estate) {
      return res.status(404).send({ message: "Estate does not found" });
    }
    const isExist = await UserEstate.findOne(
      { _id: userId },
      { savedEstate: { $elemMatch: { _id: estate_id } } }
    );
    // console.log(isExist.savedEstate)
    if (isExist.savedEstate.length > 0) {
      console.log("Is exist");
      return res
        .status(400)
        .send({ message: "Estate is exist in user safe list" });
    }
    const user = await UserEstate.findByIdAndUpdate(
      userId,
      {
        $push: { savedEstate: estate },
      },
      { new: true }
    );
    if (!user) {
      return res.status(404).send({ message: "User not found" });
    }
    await user.save();
    res.status(200).json(user.savedEstate);
  } catch (error) {
    return res.status(500).send({ message: "Server Error safe estate" });
  }
};
//  get userst for chats

export const getUsers = async (req: Request, res: Response) => {
  try {
    console.log(req.userId);
    const users = await UserEstate.find({ _id: { $ne: req.userId } }).select(
      "-password"
    );
    res.status(200).json(users);
  } catch (error) {
    res.status(500).json({ message: "Server error" });
  }
};
