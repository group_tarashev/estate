import { Request, Response } from "express";
import Estate, { EstateType } from "../models/estate";
import { uplaodImage } from "../middleware/cloudinary";
import { CallbackError, SortOrder } from "mongoose";
import UserEstate, { UserType } from "../models/user";

export const addEstate = async (req: Request, res: Response) => {
  // console.log(req.body);
  // console.log(req.files);
  try {
    const id = req.userId;
    const {
      name,
      baths,
      description,
      type,
      address,
      sell,
      etaj,
      kv,
      water,
      power,
      rent,
      mate,
      parking,
      furnished,
      offer,
      beds,
      price,
    } = req.body;
    const user = await UserEstate.findOne({ _id: id })
    const images = req.files as Express.Multer.File[];
    const imagesUrl = await uplaodImage(images);
    // console.log(imagesUrl);
    const estate = new Estate({
      name,
      baths,
      description,
      type,
      address,
      sell,
      kv,
      etaj,
      water,
      power,
      rent,
      mate,
      parking,
      furnished,
      offer,
      beds,
      price : parseInt(price),
      imagesUrls: imagesUrl,
      userId: id,
      userTel: user.tel
    });
    await estate.save();
    return res.status(200).json(estate);
  } catch (error) {
    return res.status(500).send({ message: error });
  }
};
export const getEstates = async (req: Request, res: Response) => {
  const estates = await Estate.find();
  return res.status(200).json(estates);
};
export const getUserEstate = async (req: Request, res: Response) => {
  const id = req.userId;
  try {
    const estate = await Estate.find({ userId: id });
    res.status(200).json(estate);
  } catch (error) {
    res.status(500).send({ message: "Server error" });
  }
};
export const getUserEstateById = async(req:Request, res:Response) =>{
  const {id} = req.params;
  try {
    const estate = await Estate.find({userId: id})
    // console.log(estate)
    return res.status(200).json(estate)
  } catch (error) {
    res.status(500).send({ message: "Server error" });
    
  }
}

export const getSavedEstate = async (req: Request, res: Response) => {
  const id = req.userId;
  // console.log(id)
  try {
    const safe = await UserEstate.findOne({ _id: id });
    // console.log(safe)
    res.status(200).json(safe.savedEstate)
  } catch (error) {
    res.status(500).send({ message: "Server error" })
  }
}
export const removeSaved = async (req: Request, res: Response) => {
  // console.log(req.params.id)
  const userId = req.userId
  try {
    // const userEstate = await UserEstate.findOne({_id: userId})

    const user = await UserEstate.findById(userId)
    const updated = await UserEstate.findByIdAndUpdate({ _id: userId }, {
      savedEstate: user.savedEstate.filter((item: any) => item._id.toString() !== req.params.id)
    }, { new: true })
    await updated.save();
    res.status(200).json(updated.savedEstate)
  } catch (error) {
    res.status(500).send({ message: "Server Error" })
  }
}
export const updateEstateById = async (req: Request, res: Response) => {
  // const estate = await Estate.findById()
  try {
    const {
      _id,
      name,
      baths,
      description,
      address,
      type,
      sell,
      rent,
      etaj,
      water,
      power,
      kv,
      mate,
      parking,
      furnished,
      offer,
      beds,
      price,
    } = req.body;
    const images = req.files as Express.Multer.File[];
    const imgUlr = await uplaodImage(images);
    const estate = await Estate.findByIdAndUpdate(
      _id,
      {
        name,
        baths,
        description,
        type,
        address,
        sell,
        rent,
        water, 
        power,
        kv,
        etaj,
        mate,
        parking,
        furnished,
        offer,
        beds,
        price :parseInt(price),
        imagesUrls: imgUlr,
      },
      { new: true }
    );
    if (estate) {
      await estate.save();
      return res.status(200).send(estate);
    } else {
      return res.status(404).json({ message: "Estate not found" });
    }
  } catch (error) {
    return res.status(500).send({ message: "Error update estate" });
  }
};
// ---------------------- Delete estate --------

export const deleteEstate = async (req: Request, res: Response) => {
  try {
    const id = req.params.id
    // console.log(id)
    const find = await Estate.findById(id);
    const users = await UserEstate.updateMany({},
      {
        $pull: { savedEstate: { $in: [find] } }
      });

    const estate = await Estate.findByIdAndDelete(req.params.id)
    if (estate) {
      res.status(200).send({ message: "Succesfull deleted" })
    } else {
      res.status(404).send({ message: "Could not delete" })
    }

  } catch (error) {
    res.status(500).send({ message: "Error with deleting estate" })
  }

}

// ---------------------- Search Estates
export const getSearch = async (req: Request, res: Response) => {
  // console.log(req.query.search);
  try {
    const search = req.query.search;
    const estates = await Estate.find({
      address: { $regex: search, $options: "i" },
    });
    return res.status(200).send(estates);
  } catch (error) {
    return res.status(500).send({ message: "Server error" });
  }
};

export const getSearchOption = async (req: Request, res: Response) => {
  try {
    // console.log(req.query)
    let { address } = req.query;
    let type = req.query.type;
    const optForm = options(req.query); // options filter
    // console.log(req.query)
    let sort: string | { $meta: any } | { [key: string]: SortOrder }
    if (typeof req.query.sort === 'string') {
      const sortOrder: number = parseInt(req.query.sort)
      sort = { price: sortOrder === 1 ? 1 : -1 }
    }
    
    // console.log(req.query.type);
    const estate = await Estate.find({
      address: { $regex: address, $options: "i" },
      type:{$regex: type, $options: 'i'},
      $or:[
        optForm
      ]
    }).sort(sort);
    return res.status(200).send(estate);
  } catch (error) {
    return res.status(500).send({ message: "Server error" });
  }
};

const options = (option: any) => {
  let optionForm = {
    rent: {},
    sell: {},
    mate: {},
    parking: {},
    offer: {},
    furnished:{},
  };
  optionForm.rent = option.rent == "true" ? true : [true, false]
  optionForm.sell = option.sell == "true" ?  true  : [true, false]
  optionForm.mate = option.mate == "true" ? true  : [true, false]
  optionForm.parking = option.parking == "true" ? true  : [true, false]
  optionForm.offer = option.offer == "true" ? true  : [true, false]
  optionForm.furnished = option.furnish == "true" ? true  : [true, false]



  // if (option.rent == "true" || option.sell === undefined) {
  //   optionForm.rent = { $in: true };
  // } else {
  //   optionForm.rent = false
  // }
  // if (option.sell == 'true' || option.sell == undefined) {
  //   optionForm.sell = { $in: true }
  // } else {
  //   optionForm.sell = false
  // }
  // if (option.parking == 'true' || option.parking == undefined) {
  //   optionForm.parking = { $in: true }
  // } else {
  //   optionForm.parking = { $in: [true, false] }
  // }
  // if (option.offer == 'true' || option.offer == undefined) {
  //   optionForm.offer = { $in: true }
  // } else {
  //   optionForm.offer = { $in: [true, false] }
  // }
  // if(option.mate == 'true' || option.mate == undefined){
  //   optionForm.mate = {$in:true}
  // }else{
  //   optionForm.mate = {$in:[true, false]}
  // }
  return optionForm;
};
